#!/usr/bin/python
import vtk
from numpy import pi
import numpy as np
import sys
import getopt
from stl import mesh

colors = vtk.vtkNamedColors()


def mkVtkIdList(it):
    """
    Makes a vtkIdList from a Python iterable. I'm kinda surprised that
     this is necessary, since I assumed that this kind of thing would
     have been built into the wrapper and happen transparently, but it
     seems not.

    :param it: A python iterable.
    :return: A vtkIdList
    """
    vil = vtk.vtkIdList()
    for i in it:
        vil.InsertNextId(int(i))
    return vil


def get_actors():
    cubeActorList = []

    # Read file
    global inputfile
    lines = np.loadtxt(inputfile, comments="#", delimiter="\t", unpack=False)

    # meshlists
    inside_mesh_vertices = []
    inside_mesh_faces = []
    outside_mesh_vertices = []
    outside_mesh_faces = []
    boundary_mesh_vertices = []
    boundary_mesh_faces = []
    for line in lines: #TODO: how to make this faster?
        xmin = line[0]
        xmax = line[1]
        ymin = line[2]
        ymax = line[3]
        zmin = line[4]*10
        zmax = line[5]*10
        classification = line[6]

        # Create box mesh
        mesh_vertices = [(xmin, ymin, zmin),
                         (xmax, ymin, zmin),
                         (xmax, ymax, zmin),
                         (xmin, ymax, zmin),
                         (xmin, ymin, zmax),
                         (xmax, ymin, zmax),
                         (xmax, ymax, zmax),
                         (xmin, ymax, zmax)]

        # Append to meshlists
        if classification == 1:
            S = 8*len(inside_mesh_vertices)
            mesh_faces = [(0 + S, 1 + S, 2 + S),
                          (0 + S, 2 + S, 3 + S),
                          (4 + S, 5 + S, 6 + S),
                          (4 + S, 6 + S, 7 + S),
                          (0 + S, 1 + S, 5 + S),
                          (0 + S, 5 + S, 4 + S),
                          (1 + S, 2 + S, 6 + S),
                          (1 + S, 6 + S, 5 + S),
                          (2 + S, 3 + S, 7 + S),
                          (2 + S, 7 + S, 6 + S),
                          (3 + S, 0 + S, 4 + S),
                          (3 + S, 4 + S, 7 + S)]
            inside_mesh_vertices.append(mesh_vertices)
            inside_mesh_faces.append(mesh_faces)
        elif classification == -1:
            S = 8*len(outside_mesh_vertices)
            mesh_faces = [(0 + S, 1 + S, 2 + S),
                          (0 + S, 2 + S, 3 + S),
                          (4 + S, 5 + S, 6 + S),
                          (4 + S, 6 + S, 7 + S),
                          (0 + S, 1 + S, 5 + S),
                          (0 + S, 5 + S, 4 + S),
                          (1 + S, 2 + S, 6 + S),
                          (1 + S, 6 + S, 5 + S),
                          (2 + S, 3 + S, 7 + S),
                          (2 + S, 7 + S, 6 + S),
                          (3 + S, 0 + S, 4 + S),
                          (3 + S, 4 + S, 7 + S)]
            outside_mesh_vertices.append(mesh_vertices)
            outside_mesh_faces.append(mesh_faces)
        else:
            S = 8*len(boundary_mesh_vertices)
            mesh_faces = [(0 + S, 1 + S, 2 + S),
                          (0 + S, 2 + S, 3 + S),
                          (4 + S, 5 + S, 6 + S),
                          (4 + S, 6 + S, 7 + S),
                          (0 + S, 1 + S, 5 + S),
                          (0 + S, 5 + S, 4 + S),
                          (1 + S, 2 + S, 6 + S),
                          (1 + S, 6 + S, 5 + S),
                          (2 + S, 3 + S, 7 + S),
                          (2 + S, 7 + S, 6 + S),
                          (3 + S, 0 + S, 4 + S),
                          (3 + S, 4 + S, 7 + S)]
            boundary_mesh_vertices.append(mesh_vertices)
            boundary_mesh_faces.append(mesh_faces)

    # Convert tuples to np arrays
    inside_mesh_vertices = np.array(inside_mesh_vertices).reshape(-1, 3)
    inside_mesh_faces = np.array(inside_mesh_faces).reshape(-1, 3)
    outside_mesh_vertices = np.array(outside_mesh_vertices).reshape(-1, 3)
    outside_mesh_faces = np.array(outside_mesh_faces).reshape(-1, 3)
    boundary_mesh_vertices = np.array(boundary_mesh_vertices).reshape(-1, 3)
    boundary_mesh_faces = np.array(boundary_mesh_faces).reshape(-1, 3)

    # Save meshes
    inside_mesh = mesh.Mesh(np.zeros(inside_mesh_faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(inside_mesh_faces):
        for j in range(3):
            inside_mesh.vectors[i][j] = inside_mesh_vertices[f[j], :]
    inside_mesh.save("data/inside_mesh.stl")
    outside_mesh = mesh.Mesh(np.zeros(outside_mesh_faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(outside_mesh_faces):
        for j in range(3):
            outside_mesh.vectors[i][j] = outside_mesh_vertices[f[j], :]
    outside_mesh.save("data/outside_mesh.stl")
    boundary_mesh = mesh.Mesh(np.zeros(boundary_mesh_faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(boundary_mesh_faces):
        for j in range(3):
            boundary_mesh.vectors[i][j] = boundary_mesh_vertices[f[j], :]
    boundary_mesh.save("data/boundary_mesh.stl")

    global cubeActor1
    global cubeActor2
    global cubeActor3

    # inside
    filename1 = "data/inside_mesh.stl"
    reader1 = vtk.vtkSTLReader()
    reader1.SetFileName(filename1)
    mapper1 = vtk.vtkPolyDataMapper()
    mapper1.SetInputConnection(reader1.GetOutputPort())
    cubeActor1 = vtk.vtkActor()
    cubeActor1.SetMapper(mapper1)
    cubeActor1.GetProperty().SetColor(colors.GetColor3d("Blue"))
    cubeActorList.append(cubeActor1)

    # outside
    filename2 = "data/outside_mesh.stl"
    reader2 = vtk.vtkSTLReader()
    reader2.SetFileName(filename2)
    mapper2 = vtk.vtkPolyDataMapper()
    mapper2.SetInputConnection(reader2.GetOutputPort())
    cubeActor2 = vtk.vtkActor()
    cubeActor2.SetMapper(mapper2)
    cubeActor2.GetProperty().SetColor(colors.GetColor3d("Red"))
    cubeActorList.append(cubeActor2)

    # boundary
    filename3 = "data/boundary_mesh.stl"
    reader3 = vtk.vtkSTLReader()
    reader3.SetFileName(filename3)
    mapper3 = vtk.vtkPolyDataMapper()
    mapper3.SetInputConnection(reader3.GetOutputPort())
    cubeActor3 = vtk.vtkActor()
    cubeActor3.SetMapper(mapper3)
    cubeActor3.GetProperty().SetColor(colors.GetColor3d("Yellow"))
    cubeActorList.append(cubeActor3)

    return cubeActorList


def key_pressed_callback(obj, event):
    # ---------------------------------------------------------------
    # Attach actions to specific keys
    # ---------------------------------------------------------------
    key = obj.GetKeySym()

    global cubeActor1
    global cubeActor2
    global cubeActor3
    global cubeActor1_visible
    global cubeActor2_visible
    global cubeActor3_visible
    global renderer
    if key == "i":
        print("toggle inside boxes")
        if cubeActor1_visible:
            renderer.RemoveActor(cubeActor1)
            cubeActor1_visible = False
        else:
            renderer.AddActor(cubeActor1)
            cubeActor1_visible = True
    elif key == "o":
        print("toggle outside boxes")
        if cubeActor2_visible:
            renderer.RemoveActor(cubeActor2)
            cubeActor2_visible = False
        else:
            renderer.AddActor(cubeActor2)
            cubeActor2_visible = True
    elif key == "b":
        print("toggle boundary boxes")
        if cubeActor3_visible:
            renderer.RemoveActor(cubeActor3)
            cubeActor3_visible = False
        else:
            renderer.AddActor(cubeActor3)
            cubeActor3_visible = True
    elif key == 'z':
        camera = vtk.vtkCamera()
        camera.SetPosition(0, 0, -1)
        camera.SetFocalPoint(0, 0, 0)
        renderer.SetActiveCamera(camera)
        renderer.ResetCamera()
    elif key == 'x':
        camera = vtk.vtkCamera()
        camera.SetPosition(-1, 0, 0)
        camera.SetFocalPoint(0, 0, 0)
        renderer.SetActiveCamera(camera)
        renderer.ResetCamera()
    elif key == 'y':
        camera = vtk.vtkCamera()
        camera.SetPosition(0, -1, 0)
        camera.SetFocalPoint(0, 0, 0)
        renderer.SetActiveCamera(camera)
        renderer.ResetCamera()
    elif key == 'v':
        camera = vtk.vtkCamera()
        camera.SetPosition(-0.5, 0.5, -1)
        camera.SetFocalPoint(0, 0, 0)
        renderer.SetActiveCamera(camera)
        renderer.ResetCamera()
    global renWin
    global cubeAxesActor
    renderer.RemoveActor(cubeAxesActor)
    cubeAxesActor = vtk.vtkCubeAxesActor()
    cubeAxesActor.SetBounds(-pi / 2 - 1, 3 * pi / 2 + 1, -pi - 1, pi + 1, 0, 10)
    cubeAxesActor.SetCamera(renderer.GetActiveCamera())
    cubeAxesActor.GetTitleTextProperty(0).SetColor(1.0, 0.0, 0.0)
    cubeAxesActor.GetLabelTextProperty(0).SetColor(1.0, 0.0, 0.0)

    cubeAxesActor.GetTitleTextProperty(1).SetColor(0.0, 0.0, 0.0)
    cubeAxesActor.GetLabelTextProperty(1).SetColor(0.0, 0.0, 0.0)

    cubeAxesActor.GetTitleTextProperty(2).SetColor(0.0, 0.0, 1.0)
    cubeAxesActor.GetLabelTextProperty(2).SetColor(0.0, 0.0, 1.0)

    cubeAxesActor.SetXTitle("Swivel angle (rad)")
    cubeAxesActor.SetYTitle("Twist angle (rad)")
    cubeAxesActor.SetZTitle("Path (10t)")

    cubeAxesActor.DrawXGridlinesOn()
    cubeAxesActor.DrawYGridlinesOn()
    cubeAxesActor.DrawZGridlinesOn()

    cubeAxesActor.XAxisMinorTickVisibilityOff()
    cubeAxesActor.YAxisMinorTickVisibilityOff()
    cubeAxesActor.ZAxisMinorTickVisibilityOff()

    cubeAxesActor.DrawXGridlinesOff()
    cubeAxesActor.DrawYGridlinesOff()
    cubeAxesActor.DrawZGridlinesOff()

    renderer.AddActor(cubeAxesActor)
    renWin.Render()


def main(argv):
    global inputfile
    inputfile = '/tmp/workspace.txt' # load default file
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile="])
    except getopt.GetoptError:
        print
        'vtk_plotter.py -i <inputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print
            'vtk_plotter.py -i <inputfile>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
    print
    'Input file is "', inputfile

    global cubeActor1_visible
    global cubeActor2_visible
    global cubeActor3_visible

    cubeActor1_visible = True
    cubeActor2_visible = True
    cubeActor3_visible = True

    global renWin
    global renderer
    global cubeActorList
    global cubeAxesActor

    # Read file to populate actors
    cubeActorList = get_actors()

    # The usual rendering stuff.
    camera = vtk.vtkCamera()
    camera.SetPosition(-0.5, 0.5, -1)
    camera.SetFocalPoint(0, 0, 0)

    renderer = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(renderer)

    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)

    # ---------------------------------------------------------------
    # Add a custom callback function to the interactor
    # ---------------------------------------------------------------
    iren.AddObserver("KeyPressEvent", key_pressed_callback)

    for cubeActor in cubeActorList:
        renderer.AddActor(cubeActor)
    renderer.SetActiveCamera(camera)
    renderer.ResetCamera()
    renderer.SetBackground(colors.GetColor3d("Grey"))
    # renderer.SetBackground(1.0, 0.9688, 0.8594)

    renWin.SetSize(600, 600)

    cubeAxesActor = vtk.vtkCubeAxesActor()
    cubeAxesActor.SetBounds(-pi/2 -1, 3*pi/2 +1, -pi-1, pi+1, 0, 10)
    cubeAxesActor.SetCamera(renderer.GetActiveCamera())
    cubeAxesActor.GetTitleTextProperty(0).SetColor(1.0, 0.0, 0.0)
    cubeAxesActor.GetLabelTextProperty(0).SetColor(1.0, 0.0, 0.0)

    cubeAxesActor.GetTitleTextProperty(1).SetColor(0.0, 0.0, 0.0)
    cubeAxesActor.GetLabelTextProperty(1).SetColor(0.0, 0.0, 0.0)

    cubeAxesActor.GetTitleTextProperty(2).SetColor(0.0, 0.0, 1.0)
    cubeAxesActor.GetLabelTextProperty(2).SetColor(0.0, 0.0, 1.0)

    cubeAxesActor.SetXTitle("Elbow swivel angle (rad)")
    cubeAxesActor.SetYTitle("Hand twist angle (rad)")
    cubeAxesActor.SetZTitle("Trajectory (10t)")

    cubeAxesActor.DrawXGridlinesOn()
    cubeAxesActor.DrawYGridlinesOn()
    cubeAxesActor.DrawZGridlinesOn()

    cubeAxesActor.XAxisMinorTickVisibilityOff()
    cubeAxesActor.YAxisMinorTickVisibilityOff()
    cubeAxesActor.ZAxisMinorTickVisibilityOff()

    cubeAxesActor.DrawXGridlinesOff()
    cubeAxesActor.DrawYGridlinesOff()
    cubeAxesActor.DrawZGridlinesOff()

    renderer.AddActor(cubeAxesActor)

    # interact with data
    renWin.Render()
    iren.Start()


if __name__ == "__main__":
    main(sys.argv[1:])
