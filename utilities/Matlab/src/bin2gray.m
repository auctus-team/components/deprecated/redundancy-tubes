% bin2gray.m
% Obtained from http://www.matrixlab-examples.com/gray-code.html
% Date: July 17, 2013
%
% Purpose: Converts a binary string to a gray code string
%

function g = bin2gray(b)
g(1) = b(1);
for i = 2 : length(b);
    x = xor(str2num(b(i-1)), str2num(b(i)));
    g(i) = num2str(x);
end