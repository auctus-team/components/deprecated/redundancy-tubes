function [sphere_radius, width_cube] = linear_transform_inscribed_sphere(A, x, xc, showplot)
    %%
    % radius = linear_transform_inscribed_sphere(A, x, xc, showplot)
    %
    % Joshua K. Pickard
    % January 10, 2020
    % 
    % To run:
    % [rs,rc] = linear_transform_inscribed_sphere(rand(3,4), infsup(-100*ones(4,1),100*ones(4,1)),[0,0,0]',true)
    % [rs,rc] = linear_transform_inscribed_sphere([0.8947   0.6707   0.2409; 0.3348   0.3899   0.6958], infsup(-1,1)*ones(3,1), [0,0]', true)
    % [rs,rc] = linear_transform_inscribed_sphere([0.8947   0.6707   0.2409; 0.3348   0.3899   0.6958] + infsup(-0.01,0.01), infsup(-1,1)*ones(3,1), [0,0]', true)
    %% Init
    assert(size(A,1) <= size(A,2));
%     close all;
    
    %% Milan polytope intersection
    if isa(A,'intval')
        % Compute r_p
        midA = mid(A);
        radx = rad(x);
        rankmidA = rank(midA);
        if rankmidA==size(midA,1)
            r_p_vec = (radx - abs((pinv(midA))) * rad(A) * mag(x)) ./ radx;
            r_p = min(r_p_vec);
            
%             r_p_vec = abs((radx - abs((pinv(midA))) * rad(A) * mag(x)) ./ radx);
%             r_p = min(r_p_vec);
            
            r_p = max(0,r_p);
        else
            r_p = 0;
        end
        A = midA;

%         % Plot with mid(A)
%         if showplot        
%             box_x = create_box(size(x,1),inf(x),sup(x));
%             P = (A * box_x')';
%             if (size(A,1)==3)
%                 [k,~] = convhulln(P);
%                 trisurf(k,P(:,1),P(:,2),P(:,3),'FaceColor','k','FaceAlpha',0.3);
%                 hold on;
% %                 scatter3(P(:,1),P(:,2),P(:,3),'bo');
%             elseif (size(A,1)==2)
%                 k = boundary(P(:,1),P(:,2),0);
%                 fill(P(k,1), P(k,2), 'k', 'FaceColor','none','FaceAlpha',0.3);
%                 hold on;
% %                 scatter(P(:,1),P(:,2),'bo');
%             end
%         end

        % Update x
        x = mid(x) + infsup(-r_p,r_p)*radx;            
%         x = mid(x) + ones(size(x))*infsup(-1,1)*r_p; 
    end
    
    
    %% Hyperplane shifting method
    % Gouttefarde M., Krut S. (2010) Characterization of Parallel Manipulator Available Wrench Set Facets. In: Lenarcic J., Stanisic M. (eds) Advances in Robot Kinematics: Motion in Man and Machine. Springer, Dordrecht
    
    % Combination of n x n-1 columns of A
    C = nchoosek(1:size(A,2),size(A,1)-1);
    
    % Nullspace of A
    sphere_radius = inf;
    width_cube = inf;
    for comb=1:size(C,1)
        W = A(:,C(comb,:));
        [U,S,V] = svd(W');
        d = size(A,1) - rank(S);
        if d==1 
            c = V(:,end);
            I = c' * A;           
            I_positive = find(I>0);
            I_negative = find(I<0);
            
            % Compute offsets
            d_positive = sum(I(I_positive) * sup(x(I_positive))) + sum(I(I_negative) * inf(x(I_negative)));
            d_negative = -sum(I(I_negative) * sup(x(I_negative))) - sum( I(I_positive) * inf(x(I_positive)));
                
            % Update radius using Chebyshev center formula
            r_positive = (d_positive - c' * xc) / norm(c,2);
            r_negative = (d_negative - (-c') * xc) / norm(-c,2);
            sphere_radius = min([sphere_radius,r_positive,r_negative]);
            
            r_positive = (d_positive - c' * xc) / norm(c,1);
            r_negative = (d_negative - (-c') * xc) / norm(-c,1);
            width_cube = min([width_cube,r_positive,r_negative]);
        end      
    end
    
    if sphere_radius==inf
        sphere_radius = 0;
    end
    if width_cube==inf
        width_cube = 0;
    end
    
    % Plot polytope and sphere
    if showplot        
        box_x = create_box(size(x,1),inf(x),sup(x));
        P = (A * box_x')';
        
        if (size(A,1)==3)
            [k,~] = convhulln(P);
            trisurf(k,P(:,1),P(:,2),P(:,3),'FaceColor','b','FaceAlpha',0.3);
        elseif (size(A,1)==2)
            k = boundary(P(:,1),P(:,2),0);
            fill(P(k,1), P(k,2), 'k', 'FaceColor', 'none','FaceAlpha',0.3);
        end
        hold on;
        
        if (size(A,1)==3)
            [k,~] = convhulln(P);
            trisurf(k,P(:,1),P(:,2),P(:,3),'FaceColor','b','FaceAlpha',0.3);
        elseif (size(A,1)==2)
            k = boundary(P(:,1),P(:,2),0);
            fill(P(k,1), P(k,2), 'b','FaceAlpha',0.3);
        end
        hold on;
        
        % Sphere        
        if (size(A,1)==3)
            [X,Y,Z] = sphere;
            X = X*sphere_radius + xc(1);
            Y = Y*sphere_radius + xc(2);
            Z = Z*sphere_radius + xc(3);
            surf(X,Y,Z, 'Facecolor','g');
        elseif (size(A,1)==2)
            th = 0:pi/50:2*pi;
            x = sphere_radius * cos(th) + xc(1);
            y = sphere_radius * sin(th) + xc(2);
            fill(x,y, 'g');  
            alpha(0.3);
        end
        axis equal;
        
        % Cube
        cube = midrad(xc,width_cube);
        S = create_box(size(cube,1),inf(cube),sup(cube));  
        if (size(A,1)==3)
            [k,~] = convhulln(S);
            trisurf(k,S(:,1),S(:,2),S(:,3),'FaceColor','m','FaceAlpha',0.3);
        elseif (size(A,1)==2)
            k = boundary(S(:,1),S(:,2),0);
            fill(S(k,1), S(k,2), 'm','FaceAlpha',0.3);
        end
    end
end