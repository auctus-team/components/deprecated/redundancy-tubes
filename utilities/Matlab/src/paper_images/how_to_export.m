xlabel('$b_1$','interpreter','latex','FontSize', 20);
ylabel('$b_2$','interpreter','latex','FontSize', 20);

xlabel('$x_1$','interpreter','latex','FontSize', 20);
ylabel('$x_2$','interpreter','latex','FontSize', 20);

xlabel('$\Delta x$','interpreter','latex','FontSize', 20);
ylabel('$\Delta y$','interpreter','latex','FontSize', 20);

xlabel('$\dot{x_1}$','interpreter','latex','FontSize', 20);
ylabel('$\dot{x_2}$','interpreter','latex','FontSize', 20);

xlabel('$f_x$','interpreter','latex','FontSize', 20);
ylabel('$f_y$','interpreter','latex','FontSize', 20);

xlabel('$\ddot{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\ddot{q}_2$','interpreter','latex','FontSize', 20);
zlabel('$\ddot{q}_3$','interpreter','latex','FontSize', 20);

axis equal

ha = annotation('textbox',[0.5 0.5 0.4 0.2], 'Interpreter', 'latex');
set(ha, 'String', '$\Sigma_{\forall\exists}\left({\bf A}, [{\bf x}]\right)$','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists}\left(\textrm{mid}([{\bf A}]), [{\bf x}]\right)$','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists}\left([{\bf A}], [{\bf x}]\right)$','FontSize', 20, 'LineStyle','none');

set(ha, 'String', '$\Sigma_{\forall\exists}\left({\bf A}, [{\bf b}]\right)$','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists}\left([{\bf A}], [{\bf b}]\right)$','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists}\left(\textrm{mid}([{\bf A}]), [{\bf b}]\right)$','FontSize', 20, 'LineStyle','none');

set(ha, 'String', '$\Sigma_{\forall\exists}\left({\bf J}([{\bf q}]), [\dot{\bf q}]\right)$','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists}\left(\textrm{mid}({\bf J}([{\bf q}])), [\dot{\bf q}]\right)$','FontSize', 20, 'LineStyle','none');


set(ha, 'String', '$\Sigma_{\forall\exists} ({\bf J}([{\bf q}])^T,[$\boldmath$\mathbf{\tau}])$ ','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists} (\textrm{mid}({\bf J}([{\bf q}])^T),[$\boldmath$\mathbf{\tau}])$ ','FontSize', 20, 'LineStyle','none');


set(ha, 'String', '$\Sigma_{\forall\exists} ({\bf J}([{\bf q}_{k+1}])^T,[$\boldmath$\mathbf{\tau}_{eff}])$ ','FontSize', 20, 'LineStyle','none');
set(ha, 'String', '$\Sigma_{\forall\exists} (\textrm{mid}({\bf J}([{\bf q}_{k+1}])^T),[$\boldmath$\mathbf{\tau}_{eff}])$ ','FontSize', 20, 'LineStyle','none');


print(gcf,'./tolsetx.png','-dpng','-r300');
print(gcf,'./tolsetAx_b_plot.png','-dpng','-r300');
print(gcf,'./tolsetAb_x_plot.png','-dpng','-r300');
print(gcf,'./tolsetJqd_xd_plot.png','-dpng','-r300');
print(gcf,'./tolsetJqd_xd_plot_3link.png','-dpng','-r300');
print(gcf,'./tolsetJtau_f_plot_3link.png','-dpng','-r300');
print(gcf,'./tolsetMtaueff_qdd_plot_3link.png','-dpng','-r300');

print(gcf,'./tolsetJtaueff_f_plot_3link.png','-dpng','-r300');
