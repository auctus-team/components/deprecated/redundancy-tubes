close all;
clear;

l1 = infsup(0.649,0.651);
l2 = infsup(1.099,1.101);
l3 = infsup(0.299,0.301);

m1 = infsup(0.099,0.101);
m2 = infsup(0.099,0.101);
m3 = infsup(0.099,0.101);

% q1 = 1.0;
% q2 = 1.0;
% q3 = 1.0;
% 
% q1 = 0.99;
% q2 = 0.99;
% q3 = 0.99;

% q1 = infsup(0.99,1.01);
% q2 = infsup(1.99,2.01);
% q3 = infsup(2.99,3.01);

q1_k = 1.0;
q2_k = 2.0;
q3_k = 3.0;

qd1_k = -2.5;
qd2_k = -2.5;
qd3_k = -2.5;

qdd1_k = 0.5;
qdd2_k = 0.5;
qdd3_k = 0.5;

q1_lim = infsup(-pi,pi);
q2_lim = infsup(-pi,pi);
q3_lim = infsup(-pi,pi);

qd1_lim = infsup(-5,5);
qd2_lim = infsup(-5,5);
qd3_lim = infsup(-5,5);

qdd1_lim = infsup(-20,20);
qdd2_lim = infsup(-20,20);
qdd3_lim = infsup(-20,20);

qddd1_lim = infsup(-200,200);
qddd2_lim = infsup(-200,200);
qddd3_lim = infsup(-200,200);

%% Future motions

t = infsup(0,0.01);

qdd1 = intersect(qdd1_k + qddd1_lim * t, qdd1_lim);
qdd2 = intersect(qdd2_k + qddd2_lim * t, qdd2_lim);
qdd3 = intersect(qdd3_k + qddd3_lim * t, qdd3_lim);

qd1 = intersect(qd1_k + qdd1_lim * t + qddd1_lim / 2 * t^2, qd1_lim);
qd2 = intersect(qd2_k + qdd2_lim * t + qddd2_lim / 2 * t^2, qd2_lim);
qd3 = intersect(qd3_k + qdd3_lim * t + qddd3_lim / 2 * t^2, qd3_lim);

q1 = intersect(q1_k + qd1_lim * t + qdd1_lim / 2 * t^2 + qddd1_lim / 6 * t^3, q1_lim);
q2 = intersect(q2_k + qd2_lim * t + qdd2_lim / 2 * t^2 + qddd2_lim / 6 * t^3, q2_lim);
q3 = intersect(q3_k + qd3_lim * t + qdd3_lim / 2 * t^2 + qddd3_lim / 6 * t^3, q3_lim);

cq1 = cos(q1);
sq1 = sin(q1);
cq2 = cos(q2);
cq3 = cos(q3);
sq2 = sin(q2);
sq3 = sin(q3);
cq12 = cos(q1+q2);
sq12 = sin(q1+q2);
cq23 = cos(q2+q3);
sq23 = sin(q2+q3);
cq123 = cos(q1+q2+q3);
sq123 = sin(q1+q2+q3);
m23 = m2 + m3;
m123 = m1 + m2 + m3;
qd12 = qd1 + qd2;
qd13 = qd1 + qd3;
qd123 = qd1 + qd2 +qd3;

K1=l1*l2*cq2*m23;
K2=l3*m3*(l1*cq23+l2*cq3);
K3=l3*m3*(l1*cq23+ 2*l2*cq3);
K4=l2^2*m23+l3^2*m3;
K5=l1*l3*m3*sq23;
K6=l2*l3*m3*sq3;
K7=l1*l2*m23*sq2;
K8=2*qd12+q2^2;

Jd11 = -cq123 * l3 * qd123 -cq12 * l2 * qd12 -cq1 * l1 * qd1;
Jd12 = -cq123 * l3 * qd123 -cq12 * l2 * qd12;
Jd13 = -cq123 * l3 * qd123;
Jd21 = -sq123 * l3 * qd123 -sq12 * l2 * qd12 -sq1 * l1 * qd1;
Jd22 = -sq123 * l3 * qd123 -sq12 * l2 * qd12;
Jd23 = -sq123 * l3 * qd123;
Jdq = [Jd11, Jd12, Jd13;
     Jd21, Jd22, Jd23];
 
Jq = [-sin(q1+q2+q3)*l3-sin(q1+q2)*l2-sin(q1)*l1, -sin(q1+q2+q3)*l3-sin(q1+q2)*l2, -sin(q1+q2+q3)*l3;
      cos(q1+q2+q3)*l3+cos(q1+q2)*l2+cos(q1)*l1, cos(q1+q2+q3)*l3+cos(q1+q2)*l2, cos(q1+q2+q3)*l3];

M = [2*K1+2*K2+l1^2*m123+K4,    K1+K3+K4,    K2+l3^2*m3; 
    K1+K3+K4,     2*l3*l2*m3*cq3+K4,    l3*l2*m3*cq3+l3^2 *m3;
    K2+l3^2 *m3,    l3*l2*m3*cq3+l3^2*m3,    l3^2*m3];

V1 = K5 * (-4 * qd123 - qd2^2-qd3^2) -K6 * (2 *qd13+4 * qd2+ qd3^2)- K7 * (2*qd12+qd2^2);
V2 = K5 * qd1^2-K6 *(2  *qd12 +4 * qd3+  qd3^2)+  K7 *qd1^2;
V3 = (K5 + K6) * qd1^2 + K6 * (2 * qd12+qd2^2);
V = [V1;V2;V3];

G = 9.81 * [cq123* l3* m3+l2* cq12* m23+l1* cq1* m123;
    cq123* l3* m3+l2* cq12* m23;
    cq123* l3* m3];
tau = [infsup(-3,3),infsup(-3,3),infsup(-3,3)]';

A = M;
b = tau - V - G;  

infsup(V)
infsup(G)

infsup(A)
infsup(b)

zeroA = zeros(size(A));
Ain = [sup(A) , -inf(A);
       -inf(A) , sup(A);
       -eye(2*size(A,2),2*size(A,2))
];
bin = [sup(b);
       -inf(b);
       zeros(2*size(A,2),1)
];

xc = [0,0,0]';
[radius,width_cube,~] = tolerance_set_inscribed_sphere(A,b,xc,false)
[~,~,x_vertices] = tolerance_set_inscribed_sphere(mid(A),b,xc,true);

close all;

vert = con2vert(Ain, bin, [0.1,0.1,0.1,0.1,0.1,0.1]'); 
x = vert(:,1:3) - vert(:,4:6);

% Append qdd limit constraints
[A2,b2] = vert2con(x);
Ain2 = [A2;
     1,0,0;
     -1,0,0;
     0,1,0;
     0,-1,0;
     0,0,1;
     0,0,-1;];
 bin2 = [b2;
      sup(qdd1_lim);
      -inf(qdd1_lim);
      sup(qdd2_lim);
      -inf(qdd2_lim);
      sup(qdd3_lim);
      -inf(qdd3_lim);];
x = con2vert(Ain2, bin2, [0.1,0.1,0.1]'); 


figure;
hold on;
k = convhulln(x(:,:), {'QJ','QR0'});
trisurf(k,x(:,1), x(:,2), x(:,3),'Edgecolor','b', 'FaceColor','c','FaceAlpha',0.1);
hold on;

[x,y,z] = sphere;
x = x*radius + xc(1);
y = y*radius + xc(2);
z = z*radius + xc(3);
surf(x,y,z, 'Facecolor','g');
alpha(0.3);

box = xc + infsup(-width_cube,width_cube);
box_verts = create_box(3,inf(box),sup(box));
plotbox(box_verts,'m','k',0.3)
xlabel('$\ddot{q}_1$','interpreter','latex','FontSize', 20);
ylabel('$\ddot{q}_2$','interpreter','latex','FontSize', 20);
zlabel('$\ddot{q}_3$','interpreter','latex','FontSize', 20);
axis equal;

view([67.9291,25.2125]);
print(gcf,'./paper_images/tolsetMtaueff_qdd_plot_3link.png','-dpng','-r300');
% 
% %% Forward acceleration problem
% qdd = box;
% 
% A1 = Jdq;
% x1 = [qd1;qd2;qd3];
% A2 = Jq;
% x2 = qdd;
% 
% figure;
% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(mid(A1), x1, [0,0]', true);
% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(inf(A1), x1, [0,0]', true);
% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(sup(A1), x1, [0,0]', true);
% 
% figure;
% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(A1, x1, [0,0]', true);
% 
% figure;
% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(A2, x2, [0,0]', true);