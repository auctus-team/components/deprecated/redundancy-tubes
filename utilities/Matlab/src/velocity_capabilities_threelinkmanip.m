% close all;

eps = infsup(-0.001,0.001);
l_1 = 0.65 +eps;
l_2 = 1.11 +eps;
l_3 = 0.3 +eps;

% q_1 = 1.0;
% q_2 = 1.0;
% q_3 = 1.0;

% q_1 = 0.99;
% q_2 = 0.99;
% q_3 = 0.99;

% q_1 = 1.01;
% q_2 = 1.01;
% q_3 = 1.01;

q_1 = infsup(0.99,1.01);
q_2 = infsup(0.99,1.01);
q_3 = infsup(0.99,1.01);
        
Jq = [-sin(q_1+q_2+q_3)*l_3-sin(q_1+q_2)*l_2-sin(q_1)*l_1, -sin(q_1+q_2+q_3)*l_3-sin(q_1+q_2)*l_2, -sin(q_1+q_2+q_3)*l_3;
      cos(q_1+q_2+q_3)*l_3+cos(q_1+q_2)*l_2+cos(q_1)*l_1, cos(q_1+q_2+q_3)*l_3+cos(q_1+q_2)*l_2, cos(q_1+q_2+q_3)*l_3];
        
qd = [infsup(-1,1),infsup(-1,1),infsup(-1,1)]';
    
A = Jq;
x = qd;    

xc = [0,0]';

% [sphere_radius, width_cube] = linear_transform_inscribed_sphere(A, x, xc, true)
[sphere_radius, width_cube] = linear_transform_inscribed_sphere(mid(A), x, xc, true)

figure;
% plot(x(:,1),x(:,2),'bo');
hold on;
k = boundary(x(:,1),x(:,2),0);
fill(x(k,1), x(k,2), 'k', 'FaceColor','b','FaceAlpha',0.3);
axis equal;

k_x_vertices = boundary(x_vertices(1,:)', x_vertices(2,:)',0);
fill(x_vertices(1,k_x_vertices), x_vertices(2,k_x_vertices), 'k', 'FaceColor','none');

th = 0:pi/50:2*pi;
x = radius * cos(th) + xc(1);
y = radius * sin(th) + xc(2);
fill(x,y, 'g'); 
alpha(0.3);

XY = [-width_cube+xc(1),width_cube+xc(2);
                    -width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),width_cube+xc(2);
                    width_cube+xc(1),-width_cube+xc(2);
                    -width_cube+xc(1),-width_cube+xc(2)];                
fill(XY(:,1),XY(:,2),'m');
alpha(0.3);
