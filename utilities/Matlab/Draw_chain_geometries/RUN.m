% Add src files to path
addpath('../src/');

% Read prediction
fid = fopen('/tmp/predicted_workspace.txt');
cac = textscan( fid, '%f%f%f%f%f%f%f%f%f%f%f%f', 'CollectOutput'  ...
            ,   true, 'Delimiter', ':;'  );
data = cac{1};
[~] = fclose( fid );

% Get number of segments
nSegments1 = data(1,1);
nSegments2 = data(1,2);
minDist = data(2,1);
nearSegment1 = data(3,:);
nearSegment2 = data(4,:);
nearSegment1=(nearSegment1(~isnan(nearSegment1)));
nearSegment2=(nearSegment2(~isnan(nearSegment2)));

close all;
figure;
hold on;
for ibox=1:(nSegments1+nSegments2)
    color='';
    if ibox>nSegments1
        if any(nearSegment2==(ibox-nSegments1))
            if minDist==0
                color='red';
            else
                color='green';
            end
        else
            color='b';
        end
    else
        if any(nearSegment1==ibox)
            if minDist==0
                color='red';
            else
                color='green';
            end
        else
            color='black';
        end
    end
    % get next box
    box = data(ibox+4,:);
    dim=3;
    Clb = [box(1),box(3),box(5)];
    Cub = [box(2),box(4),box(6)];
    Cbox1 = create_box(dim,Clb,Cub);    
    plotbox(Cbox1,'none',color);   
    Clb = [box(7),box(9),box(11)];
    Cub = [box(8),box(10),box(12)];
    Cbox2 = create_box(dim,Clb,Cub);
    plotbox(Cbox2,'none',color); 
    Allpts = unique(round([Cbox1;Cbox2], 3, 'significant'),'rows');
    try
        K = convhulln(Allpts);
        trisurf(K,Allpts(:,1),Allpts(:,2),Allpts(:,3),'Facecolor',color,...
        'Edgecolor',color,'FaceAlpha',0.5);
    catch            
    end
end

xlabel('x (m)', 'interpreter', 'latex');
ylabel('y (m)', 'interpreter', 'latex');
zlabel('z (m)', 'interpreter', 'latex');
view(45,45);
hblack = line(0, 0, 'Marker', 'none', 'Color', 'black');
hblue = line(0, 0, 'Marker', 'none', 'Color', 'blue');
hred = line(0, 0, 'Marker', 'none', 'Color', 'red');
hgreen = line(0, 0, 'Marker', 'none', 'Color', 'green');
dim = [.1 .6 .3 .3];
annotation('textbox',dim,'String',...
    strcat('Distance (m):  ',num2str(minDist)),...
    'FitBoxToText','on');

legend([hblack, hblue, hred, hgreen], 'robot prediction',...
    'arm prediction',...
    'colliding links',...
    'closest links',...
    'Location','northeast');
drawnow;