/*
 * ROS_redundancy_IK_solver.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <utility>
#include "LinearAlgebra.h"
#include <ctime>
#include <stdlib.h>     /* exit, EXIT_FAILURE */
#include <unistd.h>


#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float32.h>
#include "std_msgs/String.h"
#include "human_arm_redundancy_modelling/Redundancy.h"
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <cmath>

#if defined(_OPENMP)
#include <omp.h>
#endif

//int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, const char* symbol);
int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol);
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2);
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index);
int max_diam_index(ibex::IntervalVector &Vector);

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi,
		int verbose);
int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box,
		int verbose);
int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose);
void redundanciesCallback(const human_arm_redundancy_modelling::Redundancy& msg);
inline ibex::IntervalMatrix InverseTransformation(ibex::IntervalMatrix &A);
static ibex::IntervalMatrix TransformationDHModified(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta);

// Shoulder quaternion constraints (To approximate Wang et al. 1998.)
int num_shoulder_points = 18;
double _shoulderphiswing[18] = {-M_PI, -2.047099962, -1.570796327, -.8868510796, -.7439121072, -.6839452473, -.3872228929, 0., .2088928627, .6224288550, .6938260469, .8868510796, 1.570796327, 2.254741574, 2.356194490, 2.563319152, 2.780739254, M_PI};
double _shoulderthetaswing[18] = {.9995080490, .8345071701, .7853981634, .9995080490, 1.742730040, 1.900344531, 2.069514319, 2.112549334, 2.091560103, 2.069514319, 1.742730040, .9995080490, .7853981634, .9995080490, 1.107148718, 1.398862614, 1.170944762, .9995080490};
ibex::IntervalVector shoulderthetaswing(1);
ibex::IntervalVector shoulderphiswing(1);

// Wrist quaternion constraints (To approximate Gehrmann at al. 2008.)
int num_wrist_points = 9;
double _wristphiswing[9] = {-M_PI, -2.356194490192345, -1.570796326794897, -0.785398163397448, 0 , 0.785398163397448, 1.570796326794897, 2.356194490192345, M_PI};
double _wristthetaswing[9] = {0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.698131700797732, 0.523598775598299, 0.785398163397448, 1.396263401595464, 0.698131700797732};
ibex::IntervalVector wristthetaswing(1);
ibex::IntervalVector wristphiswing(1);

// Zero transforms required to compute joint angles
ibex::Interval d3(28.74,28.74);
ibex::Interval d5=(27.15,27.15);
ibex::Interval d8=(3.76,3.76);

// Variables not to be bisected.
std::vector<std::string> do_not_bisect_list = 	{"d3",
												"d5",
												"d8",
												"phi",
												"psi",
												"tX",
												"tY",
												"tZ"};

// VARIABLE LOOKUP TABLE //
std::vector<int> do_not_bisect_list_indices;

std::vector<ibex::IntervalMatrix> listOfZeroTransforms;

std::string GetCurrentWorkingDir( void ) {
  char buff[FILENAME_MAX];
  getcwd( buff, FILENAME_MAX );
  std::string current_working_dir(buff);
  return current_working_dir;
}

int main(int argc, char** argv) {
	// Get directory
	std::cout << "Working directory : " << GetCurrentWorkingDir() << std::endl;

	// ROS node initialization
	ros::init(argc, argv, "redundancy_IK_solver");
	ros::NodeHandle node;

	// ROS subscribers
	ros::Subscriber redundancies_sub = node.subscribe("redundancies", 1, redundanciesCallback);

	// Initialize kinematics
	ibex::IntervalMatrix T01 = TransformationDHModified(0,0,0,0);
	listOfZeroTransforms.push_back(T01);
	ibex::IntervalMatrix T12 = TransformationDHModified(0,M_PI/2,0,0);
	listOfZeroTransforms.push_back(T12);
	ibex::IntervalMatrix T23 = TransformationDHModified(0,-M_PI/2,d3,0);
	listOfZeroTransforms.push_back(T23);
	ibex::IntervalMatrix T34 = TransformationDHModified(0,M_PI/2,0,0);
	listOfZeroTransforms.push_back(T34);
	ibex::IntervalMatrix T45 = TransformationDHModified(0,-M_PI/2,d5,0);
	listOfZeroTransforms.push_back(T45);
	ibex::IntervalMatrix T56 = TransformationDHModified(0,M_PI/2,0,0+M_PI/2);
	listOfZeroTransforms.push_back(T56);
	ibex::IntervalMatrix T67 = TransformationDHModified(0,M_PI/2,0,0-M_PI/2);
	listOfZeroTransforms.push_back(T67);
	ibex::IntervalMatrix T78 = TransformationDHModified(0,-M_PI/2,d8,0-M_PI/2);
	listOfZeroTransforms.push_back(T78);

	// Shoulder and wrist quaternion constraints
	shoulderthetaswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderthetaswing));
	shoulderphiswing = ibex::IntervalVector(ibex::Vector(num_shoulder_points, _shoulderphiswing));
	wristthetaswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristthetaswing));
	wristphiswing = ibex::IntervalVector(ibex::Vector(num_wrist_points, _wristphiswing));

	// ROS spin
	ros::spin();

	return 0;
}

/*
 * ROS subscriber callback
 */
void redundanciesCallback(const human_arm_redundancy_modelling::Redundancy& msg)
{	
	// Verbose flag (0: warnings only, 1: general messages, 2: more information)
	int verbose = -1;

    /* ################################################
    LOAD SYSTEMS FROM MINIBEX FILES
    ################################################ */
    ibex::System system_kinematics("../../../src/human_arm_redundancy_modelling/minibex/kinematics_ZYX.mbx");
    ibex::System system_shoulder_axial("../../../src/human_arm_redundancy_modelling/minibex/shoulder_axial.mbx");
    ibex::System system_shoulder_swing("../../../src/human_arm_redundancy_modelling/minibex/shoulder_swing.mbx");
    ibex::System system_hand_swing("../../../src/human_arm_redundancy_modelling/minibex/hand_swing.mbx");

	// CONTRACTORS
	// KINEMATICS //
	// Build contractors (rebuilding in loop improves contraction performance)
	ibex::CtcHC4 hc4_kinematics(system_kinematics,0.01);
	ibex::CtcHC4 hc4acid_kinematics(system_kinematics,0.1,true);
	ibex::CtcAcid acid_kinematics(system_kinematics, hc4acid_kinematics);
	ibex::CtcCompo kinematics_contractor(hc4_kinematics,acid_kinematics);

	// SHOULDER AXIAL ROTATION //
	// Build contractors
	ibex::CtcHC4 hc4_shoulder_axial(system_shoulder_axial,0.01);
	ibex::CtcHC4 hc4acid_shoulder_axial(system_shoulder_axial,0.1,true);
	ibex::CtcAcid acid_shoulder_axial(system_shoulder_axial, hc4acid_shoulder_axial);
	ibex::CtcCompo shoulder_axial_contractor(hc4_shoulder_axial,acid_shoulder_axial);

	// SHOULDER SWING //
	// Build contractors
	ibex::CtcHC4 hc4_shoulder_swing(system_shoulder_swing,0.01);
	ibex::CtcHC4 hc4acid_shoulder_swing(system_shoulder_swing,0.1,true);
	ibex::CtcAcid acid_shoulder_swing(system_shoulder_swing, hc4acid_shoulder_swing);
	ibex::CtcCompo shoulder_swing_contractor(hc4_shoulder_swing,acid_shoulder_swing);

	// HAND SWING //
	// Build contractors
	ibex::CtcHC4 hc4_hand_swing(system_hand_swing,0.01);
	ibex::CtcHC4 hc4acid_hand_swing(system_hand_swing,0.1,true);
	ibex::CtcAcid acid_hand_swing(system_hand_swing, hc4acid_hand_swing);
	ibex::CtcCompo hand_swing_contractor(hc4_hand_swing,acid_hand_swing);

	// VARIABLE LOOKUP TABLE //
	std::vector<int> do_not_bisect_list_indices;
	for (int var_i=0; var_i<do_not_bisect_list.size(); var_i++)
		do_not_bisect_list_indices.push_back(find_var_index(system_kinematics.f_ctrs.args(), do_not_bisect_list[var_i]));

	/* ################################################
	EVALUATE IK FOR REDUNDANT ANGLES
	################################################ */
	// Get angles
	ibex::IntervalVector psiphi_(2);
	psiphi_[0] = msg.elbow_swivel;
	psiphi_[1] = msg.hand_twist;

	// KINEMATICS//
	ibex::IntervalVector kinematics_box = system_kinematics.box;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "psi")] = psiphi_[0];
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "phi")] = psiphi_[1];
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "tX")] = msg.twist_point.x;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "tY")] = msg.twist_point.y;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "tZ")] = msg.twist_point.z;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "AX")] = msg.twist_axis.x;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "AY")] = msg.twist_axis.y;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "AZ")] = msg.twist_axis.z;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "gX")] = msg.twist_refaxis.x;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "gY")] = msg.twist_refaxis.y;
	kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "gZ")] = msg.twist_refaxis.z;

	// List to store kinematics variables
	std::vector<ibex::IntervalVector> kinematic_list;
	std::vector<ibex::IntervalVector> kinematic_list_inside;
	kinematic_list.clear();
	kinematic_list.push_back(kinematics_box);
	ibex::IntervalVector psipsi_simp(2);
	// Vector used for bisection (some elements are set to zero to avoid bisection)
	ibex::IntervalVector kinematics_box_bisection(kinematics_box.size());
	int boundary_size = 0;
	int solution_size = 0;
	int num_bisections = 0;
	int num_nosolutions = 0;
	int kinematics_verified = 0;
	int shoulder_axial_rotation_verified = 0;
	int shoulder_swing_verified = 0;
	int hand_swing_verified = 0;
	bool verify_shoulder_axial = true;
	bool verify_shoulder_swing = true;
	bool verify_hand_swing = true;
	int classification = 0;
	int bisect_index;

	// VERIFICATION LOOP //
	while(kinematic_list.size()>0 && num_bisections<5 && boundary_size<1 && num_nosolutions<1){
		/* ################################################
		CHECK VERIFICATION OF KINEMATICS
		################################################ */
		kinematics_verified = 0;
		// Contract
		kinematics_contractor.contract(kinematic_list.back());
		kinematics_contractor.contract(kinematic_list.back());
		kinematics_contractor.contract(kinematic_list.back());
		kinematics_contractor.contract(kinematic_list.back());
		kinematics_contractor.contract(kinematic_list.back());

		// Make sure not empty (empty means no kinematic solution)
		if (kinematic_list.back().is_empty()){
			if (verbose > 0) std::cout << "kinematics_box empty" << std::endl;
			kinematics_verified = -2; // treated differently from following constraints
		} else {
			// Check if phi and psi were modified by simplification (TODO: prevent this in the contractions)
			psipsi_simp[0] = kinematic_list.back()[find_var_index(system_kinematics.f_ctrs.args(), "psi")];
			psipsi_simp[1] = kinematic_list.back()[find_var_index(system_kinematics.f_ctrs.args(), "phi")];
			if (psipsi_simp==psiphi_)
				kinematics_verified = kinematics_existence(system_kinematics, kinematic_list.back(), verbose);
			else{
				if (verbose > 0) std::cout << "kinematics_box simplified angles" << std::endl;
				kinematics_verified = 0;
			}
		}

		/* ################################################
		CHECK VERIFICATION OF CONSTRAINTS
		################################################ */
		shoulder_axial_rotation_verified = 0;
		shoulder_swing_verified = 0;
		hand_swing_verified = 0;

		// SHOULDER AXIAL ROTATION //
		if (verify_shoulder_axial && kinematics_verified >= 0){
			if (verbose > 0) std::cout << "SHOULDER AXIAL ROTATION" << std::endl;
			shoulder_axial_rotation_verified = verify_shoulder_axial_rotation(system_shoulder_axial,
				shoulder_axial_contractor, system_kinematics, kinematic_list.back(), psiphi_[0], verbose);
		} else {
			// Default value
			shoulder_axial_rotation_verified = 1;
		}

		// SHOULDER SWING //
		if (verify_shoulder_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0) {
			// SHOULDER SWING //
			if (verbose > 0) std::cout << "SHOULDER SWING" << std::endl;
			shoulder_swing_verified = verify_swing(shoulderthetaswing, shoulderphiswing,
					system_shoulder_swing, shoulder_swing_contractor,
					system_kinematics, kinematic_list.back(), verbose);
		} else {
			// Default value
			shoulder_swing_verified = 1;
		}

		// HAND SWING //
		if (verify_hand_swing && kinematics_verified >= 0 && shoulder_axial_rotation_verified >= 0 && shoulder_swing_verified >= 0) {
			// HAND SWING //
			if (verbose > 0) std::cout << "HAND SWING" << std::endl;
			hand_swing_verified = verify_swing(wristthetaswing, wristphiswing,
					system_hand_swing, hand_swing_contractor,
					system_kinematics, kinematic_list.back(), verbose);
		} else {
			// Default value
			hand_swing_verified = 1;
		}

		// Determine classification
		if (kinematics_verified == -2)
			classification = -2;
		else if (kinematics_verified == 1 &&
				shoulder_axial_rotation_verified == 1 &&
				shoulder_swing_verified ==1 &&
				hand_swing_verified == 1)
			classification = 1;
		else if (kinematics_verified == -1 ||
				shoulder_axial_rotation_verified == -1 ||
				shoulder_swing_verified == -1 ||
				hand_swing_verified == -1)
			classification = -1;
		else
			classification = 0;

		// BISECTION
		if (classification == -2){
			// There does not exist a solution for the kinematics (does not count towards final classification)
			if (verbose > 0) std::cout << "NO SOLUTION - kinematics empty" << std::endl;
			kinematic_list.pop_back();
		}else if (classification == -1){
			// There does not exist a solution for all of the constraints (does not count towards final classification)
			if (verbose > 0) std::cout << "NO SOLUTION - kinematics" << std::endl;
			num_nosolutions++;
			kinematic_list.pop_back();
		}else if (classification == 1){
			if (verbose > 0) std::cout << "SOLUTION - kinematics" << std::endl;
			solution_size++;
			ibex::IntervalVector inside_box = kinematic_list.back();
			kinematic_list_inside.push_back(inside_box);
			kinematic_list.pop_back();
		}else if (classification == 0){
			// bisect kinematics_box along rotation matrix elements
			kinematics_box_bisection = kinematic_list.back();
			// Variables not to be bisected.
			for (int var_i=0; var_i<do_not_bisect_list_indices.size(); var_i++)
				kinematics_box_bisection[do_not_bisect_list_indices[var_i]] = 0;

			if (kinematics_box_bisection.max_diam() > 0.001){
				bisect_index = max_diam_index(kinematics_box_bisection);
				std::pair<ibex::IntervalVector,ibex::IntervalVector> bisected = bisect_at(kinematic_list.back(), bisect_index);
				kinematic_list.pop_back();
				// append bisected to list
				kinematic_list.push_back(bisected.first);
				kinematic_list.push_back(bisected.second);
				num_bisections++;
				if (verbose > 1) std::cout << "BISECT - kinematics: " << system_kinematics.f_ctrs.args()[bisect_index] << '\t' << kinematics_box_bisection[bisect_index] << std::endl;
			} else {
				if (verbose > 0) std::cout << "BOUNDARY - kinematics" << std::endl;
				boundary_size++;
				kinematic_list.pop_back();
			}
		}
	}

	// Add any remaining kinematic boxes to boundary list
	if (verbose >= 0) std::cout << "Number of solutions: " << solution_size << std::endl;
	if (verbose >= 0) std::cout << "Number of boundaries: " << boundary_size << std::endl;
	if (verbose >= 0) std::cout << "Number of unclassified: " << kinematic_list.size() << std::endl;
	if (verbose >= 0) std::cout << "Number of no solution: " << num_nosolutions << std::endl;

	/* ################################################
	PUBLISH JOINT ANGLES
	################################################ */
	if (kinematic_list_inside.size()>0){
		ibex::IntervalVector inside_box = kinematic_list_inside.back();

		// Generate transformation matrices
		std::vector<ibex::IntervalMatrix> listOfTransforms;
		int n = 7;
		for (int i=1; i<=n; i++){
			ibex::Interval Rux = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rux" + std::to_string(i))];
			ibex::Interval Ruy = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Ruy" + std::to_string(i))];
			ibex::Interval Ruz = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Ruz" + std::to_string(i))];
			ibex::Interval Rvx = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rvx" + std::to_string(i))];
			ibex::Interval Rvy = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rvy" + std::to_string(i))];
			ibex::Interval Rvz = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rvz" + std::to_string(i))];
			ibex::Interval Rwx = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rwx" + std::to_string(i))];
			ibex::Interval Rwy = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rwy" + std::to_string(i))];
			ibex::Interval Rwz = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "Rwz" + std::to_string(i))];
			ibex::Interval rx = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "rx" + std::to_string(i))];
			ibex::Interval ry = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "ry" + std::to_string(i))];
			ibex::Interval rz = inside_box[find_var_index(system_kinematics.f_ctrs.args(), "rz" + std::to_string(i))];
			ibex::IntervalMatrix Transform(4,4);
			Transform[0][0] = Rux;
			Transform[1][0] = Ruy;
			Transform[2][0] = Ruz;
			Transform[0][1] = Rvx;
			Transform[1][1] = Rvy;
			Transform[2][1] = Rvz;
			Transform[0][2] = Rwx;
			Transform[1][2] = Rwy;
			Transform[2][2] = Rwz;
			Transform[0][3] = rx;
			Transform[1][3] = ry;
			Transform[2][3] = rz;
			Transform[3][0] = 0;
			Transform[3][1] = 0;
			Transform[3][2] = 0;
			Transform[3][3] = 1;
//			std::cout << "Transform" << std::endl;
//			std::cout << "T" << 0 << ',' << i << std::endl;
//			std::cout << Transform << std::endl;
			listOfTransforms.push_back(Transform);
		}

		// Publish
		ros::NodeHandle node;
		ros::Publisher joint_pub = node.advertise<sensor_msgs::JointState>("joint_states", 1);
		sensor_msgs::JointState joint_state;
		joint_state.header.stamp = ros::Time::now();
		joint_state.name.resize(7);
		joint_state.position.resize(7);

		// Solve for joint angles using exponential matrices
		for (int joint=0; joint<=n-1; joint++){
			ibex::IntervalMatrix T_0_i = listOfTransforms[joint];
			ibex::IntervalMatrix T_im1_i = T_0_i;
			if (joint > 0){
				// Transforms i,i+1
				ibex::IntervalMatrix T_0_im1 = listOfTransforms[joint-1];
				T_im1_i = InverseTransformation(T_0_im1) * T_0_i;
			}

			// Zero Transform T_i_prev_ZERO
			ibex::IntervalMatrix T_im1_i_ZERO = listOfZeroTransforms[joint];

			// Compute expontential matrix
			ibex::IntervalMatrix exponentialMatrix = InverseTransformation(T_im1_i) * T_im1_i_ZERO;


			// Paden-Kahan subproblem 1:
			ibex::IntervalVector pointR_3(3, 0); // Point on twist axis
			pointR_3[2] = -1;
			ibex::IntervalVector pointP_3(3, 0); // Point not on twist axis
			pointP_3[1] = 1;
			ibex::IntervalVector pointP_4(4,1); // Used for multiplication with exponentialMatrix
			pointP_4[0] = pointP_3[0];
			pointP_4[1] = pointP_3[1];
			pointP_4[2] = pointP_3[2];
			ibex::IntervalVector pointQ_4 = exponentialMatrix * pointP_4; // Point not on twist axis
			ibex::IntervalVector pointQ_3 = pointQ_4.subvector(0,2);
			ibex::IntervalVector vectorU = pointP_3 - pointR_3;
			ibex::IntervalVector vectorV = pointQ_3 - pointR_3;
			ibex::IntervalVector vectorW(3, 0); // Unit vector along twist axis
			vectorW[2] = 1;
			ibex::IntervalMatrix matrixW(3,1);
			matrixW[0][0] = vectorW[0];
			matrixW[1][0] = vectorW[1];
			matrixW[2][0] = vectorW[2];
			ibex::IntervalVector vectorU2 = vectorU - matrixW * matrixW.transpose() * vectorU; // Projected onto XY plane
			ibex::IntervalVector vectorV2 = vectorV - matrixW * matrixW.transpose() * vectorW; // Projected onto XY plane

			ibex::Interval tan2y = vectorW*(ibex::cross(vectorU2,vectorV2));
			ibex::Interval tan2x = vectorU2*vectorV2;
			ibex::Interval theta = atan2(tan2y, tan2x);
//			std::cout << "tan2y: " << tan2y << " tan2x" << tan2x  << std::endl;
//			std::cout << "theta: " << joint+1 << theta << "rads\t" << theta*180/M_PI << "degs" << std::endl;

			joint_state.name[joint] ="q" + std::to_string(joint+1);
			joint_state.position[joint] = -theta.mid();

		}
		joint_pub.publish(joint_state);
		/*ROS_INFO("FOUND SOLUTION FOR: ");
		ROS_INFO("elbow_swivel: [%f]", msg.elbow_swivel);
		ROS_INFO("hand_twist: [%f]", msg.hand_twist);
		ROS_INFO("twist_point: [%f,%f,%f]", msg.twist_point.x, msg.twist_point.y, msg.twist_point.z);
		ROS_INFO("twist_axis: [%f,%f,%f]", msg.twist_axis.x, msg.twist_axis.y, msg.twist_axis.z);
		ROS_INFO("twist_refaxis: [%f,%f,%f]", msg.twist_refaxis.x, msg.twist_refaxis.y, msg.twist_refaxis.z);
		ROS_INFO("joint_state.position: [%f,%f,%f,%f,%f,%f,%f]",
				joint_state.position[0],
				joint_state.position[1],
				joint_state.position[2],
				joint_state.position[3],
				joint_state.position[4],
				joint_state.position[5],
				joint_state.position[6]);*/
		ros::spin();
	}
	else {
		/*ROS_INFO("NO SOLUTION FOR: ");
		ROS_INFO("elbow_swivel: [%f]", msg.elbow_swivel);
		ROS_INFO("hand_twist: [%f]", msg.hand_twist);
		ROS_INFO("twist_point: [%f,%f,%f]", msg.twist_point.x, msg.twist_point.y, msg.twist_point.z);
		ROS_INFO("twist_axis: [%f,%f,%f]", msg.twist_axis.x, msg.twist_axis.y, msg.twist_axis.z);
		ROS_INFO("twist_refaxis: [%f,%f,%f]", msg.twist_refaxis.x, msg.twist_refaxis.y, msg.twist_refaxis.z);*/
		ros::spin();
	}
}

inline ibex::IntervalMatrix TransformationDHModified(ibex::Interval a, ibex::Interval alpha, ibex::Interval d, ibex::Interval theta){
	ibex::IntervalMatrix Transformation(4,4);
	Transformation[0][0] = cos(theta);
	Transformation[0][1] = -sin(theta);
	Transformation[0][2] = 0;
	Transformation[0][3] = a;
	Transformation[1][0] = sin(theta)*cos(alpha);
	Transformation[1][1] = cos(theta)*cos(alpha);
	Transformation[1][2] = -sin(alpha);
	Transformation[1][3] = -d*sin(alpha);
	Transformation[2][0] = sin(theta)*sin(alpha);
	Transformation[2][1] = cos(theta)*sin(alpha);
	Transformation[2][2] = cos(alpha);
	Transformation[2][3] = d*cos(alpha);
	Transformation[3][0] = 0;
	Transformation[3][1] = 0;
	Transformation[3][2] = 0;
	Transformation[3][3] = 1;
	return Transformation;
}

inline ibex::IntervalMatrix InverseTransformation(ibex::IntervalMatrix &A) {
	ibex::IntervalMatrix inverseTransform = A;
	// Orientation
	ibex::IntervalMatrix Ainv = A.submatrix(0,2,0,2).transpose();
	inverseTransform.put(0,0,Ainv);
	// Position
	ibex::IntervalVector P = A.col(3).subvector(0,2);
	inverseTransform.put(0,3, -Ainv*P,false);


	return inverseTransform;
}

int find_var_index(const ibex::Array<const ibex::ExprSymbol> &variable_list, std::string symbol){
	const char *cstr = symbol.c_str();
	for (int var_i=0; var_i<variable_list.size(); var_i++){
//		std::cout << variable_list[var_i].name << '\t' << symbol << std::endl;
		if (strcmp(variable_list[var_i].name, cstr) == 0){
//			std::cout << "Found index: " << var_i << std::endl;
			return var_i;
		}
	}
	return -1;
}

/*
 * Update box 2 with the values from box1 by matching the variables.
 */
void copy_vars(const ibex::Array<const ibex::ExprSymbol> &variable_list1, ibex::IntervalVector &box1,
		const ibex::Array<const ibex::ExprSymbol> &variable_list2, ibex::IntervalVector &box2){

	for (int var_i=0; var_i<variable_list1.size(); var_i++){
		for (int var_j=0; var_j<variable_list2.size(); var_j++){
//			std::cout << variable_list1[var_i].name << '\t' << variable_list2[var_j].name << std::endl;
			if (strcmp(variable_list1[var_i].name, variable_list2[var_j].name) == 0){
//				std::cout << "Match found.. updating box2" << std::endl;
				box2[var_j] = box1[var_i];
			}
		}
	}
}

/*
 * Bisect interval vector at index.
 */
std::pair<ibex::IntervalVector,ibex::IntervalVector> bisect_at(ibex::IntervalVector &Vector, int index){

	ibex::IntervalVector Vector1 = Vector;
	Vector1[index] = ibex::Interval(Vector[index].lb(),Vector[index].mid());
	ibex::IntervalVector Vector2 = Vector;
	Vector2[index] = ibex::Interval(Vector[index].mid(),Vector[index].ub());
	std::pair<ibex::IntervalVector,ibex::IntervalVector> pair = std::make_pair(Vector1,Vector2);
	return pair;
}

int max_diam_index(ibex::IntervalVector &Vector){
	ibex::Vector Diams = Vector.diam();
	int max_index = 0;
	double max_diam = Diams[max_index];
	for (int i=1; i<Diams.size(); i++){
		if (Diams[i] > Diams[max_index]){
			max_diam = Diams[i];
			max_index = i;
		}
	}
	return max_index;
}

int verify_shoulder_axial_rotation(ibex::System &axial_system, ibex::CtcCompo &axial_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, ibex::Interval &psi, int verbose){

	//ibex::IntervalVector shoulder_axial_box = axial_system.box;
	//copy_vars(kinematics_system.f_ctrs.args(), kinematics_box,
	//		axial_system.f_ctrs.args(), shoulder_axial_box);

	// Evaluate alpha and beta without constraints on limits for comparison with system box after contraction
	ibex::Interval rx4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rx4")];
	ibex::Interval ry4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "ry4")];
	ibex::Interval rz4 = kinematics_box[find_var_index(kinematics_system.f_ctrs.args(), "rz4")];
	ibex::Interval beta = atan2(rz4, sqrt(sqr(rx4)+sqr(ry4)));
	ibex::Interval alpha = atan2(rx4, -ry4);

	// Contract
	//axial_contractor.contract(shoulder_axial_box);

/*
	// Check if alpha and beta are not empty
	ibex::Interval alpha_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "alpha")];
	ibex::Interval beta_ = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "beta")];

	if (alpha_.is_empty() || beta_.is_empty()){
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta empty" << std::endl;
		return -1;
	} else 
*/
	if (alpha.ub() < -1.7453 || alpha.lb() > 2.2689 || beta.ub() < -1.3963 || beta.lb() > 1.3963){
		// alpha, beta limits dissatisfied
		if (verbose > 0) std::cout << "shoulder_axial not valid: alpha/beta range" << std::endl;
		return -1;
	} else {
		// Check constraint satisfaction: psi < EXT; psi > INT;
		//ibex::Interval EXT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "EXT")];
		//ibex::Interval INT = shoulder_axial_box[find_var_index(axial_system.f_ctrs.args(), "INT")];
		ibex::Interval beta4 = pow(beta,4);
		ibex::Interval beta3 = pow(beta,3);
		ibex::Interval beta2 = pow(beta,2);
		ibex::Interval cbeta = cos(beta);
		ibex::Interval cbeta4 = pow(cbeta,4);
		ibex::Interval cbeta3 = pow(cbeta,3);
		ibex::Interval cbeta2 = pow(cbeta,2);
		ibex::Interval alpha4 = pow(alpha,4);
		ibex::Interval alpha3 = pow(alpha,3);
		ibex::Interval alpha2 = pow(alpha,2);
		ibex::Interval A0 = alpha*cbeta;
		ibex::Interval A1 = alpha2*cbeta2;
		ibex::Interval A2 = alpha3*cbeta3;
		ibex::Interval A3 = alpha4*cbeta4;
		ibex::Interval EXT = M_PI + M_PI/180 * (0.724559*beta4+(11.73726*A0-1.535318)*beta3+(-5.683932*A1+23.970675*A0-7.366772)*beta2+(-4.558201*A2-2.565833*A1+29.532220*A0-15.71531)*beta+.331*A3-2.987*A2+3.428*A1+9.299*A0-2.459);
		ibex::Interval INT = M_PI + M_PI/180 * (3.417002*beta3+(20.882835*A0-2.676168)*beta2+(-.143589*A1+29.671092*A0-41.687220)*beta-2.081*A2+4.092*A1+18.652*A0-139.27);

		if (verbose > 0) std::cout << "axial_rotation " << INT << '\t' << psi << '\t' << EXT << std::endl;
		if (psi.ub() < EXT.lb() && psi.lb() > INT.ub() &&
				alpha.lb() >= -1.7453 && alpha.ub() <= 2.2689 && beta.lb() >= -1.3963 && beta.ub() <= 1.3963){
			// alpha, beta limits and INT, EXT limits satisfied
			if (verbose > 0) std::cout << "shoulder_axial valid" << std::endl;
			return 1;
		} else if (psi.lb() > EXT.ub() || psi.ub() < INT.lb()){
			if (verbose > 0) std::cout << "shoulder_axial not valid" << std::endl;
			return -1;
		} else{
			// alpha, beta limits or INT, EXT limits partially satisfied
			if (verbose > 0) std::cout << "shoulder_axial partially valid" << std::endl;
			return 0;
		}
	}
}

int verify_swing(ibex::IntervalVector &thetaswing, ibex::IntervalVector &phiswing,
		ibex::System &swing_system, ibex::CtcCompo &swing_contractor,
		ibex::System &kinematics_system, ibex::IntervalVector &kinematics_box, int verbose){
	int num_inside_classifications = 0;
	int num_outside_classifications = 0;
	int shoulder_swing_pair_verified = 0;
	bool has_boundary = false;
	ibex::IntervalVector swing_box_orig = swing_system.box;
	copy_vars(kinematics_system.f_ctrs.args(), kinematics_box, swing_system.f_ctrs.args(), swing_box_orig);
	for (int pair=0; pair<thetaswing.size()-1; pair++){

		// Loop over t in [0,1] until swing constraint is satisfied for all t or all t are unsatisfied
		std::vector<ibex::Interval> t_list;
		t_list.push_back(ibex::Interval(0,1));
		int num_pair_inside_classifications = 0;
		int num_pair_outside_classifications = 0;
		ibex::IntervalVector swing_box = swing_box_orig;
		ibex::Interval t,t_orig,K,G;
		while(t_list.size()>0 && !has_boundary){
			// Pop t
			t = t_list.back();
			t_list.pop_back();

			// Copy t to check solution existence after filtering
			t_orig = t;

			// Update box
			swing_box = swing_box_orig;

			// Update theta and gamma values for quaternion pair
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta1")] = thetaswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma1")] = phiswing[pair];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "theta2")] = thetaswing[pair+1];
			swing_box[find_var_index(swing_system.f_ctrs.args(), "gamma2")] = phiswing[pair+1];

			// Set t value
			swing_box[find_var_index(swing_system.f_ctrs.args(), "t")] = t;

			// Contract
			swing_contractor.contract(swing_box);

			// Get simplified t value
			t = swing_box[find_var_index(swing_system.f_ctrs.args(), "t")];
			if (verbose > 1) std::cout << "t_orig " << t_orig << " t " << t << '\t' << std::endl;

			// Check constraint G <= K <= 1
			K = swing_box[find_var_index(swing_system.f_ctrs.args(), "K")];
			G = swing_box[find_var_index(swing_system.f_ctrs.args(), "G")];

			if (verbose > 1) std::cout << "swing: " << pair << '\t' << G << '\t' << K << '\t' << 1 << std::endl;

			// Handle empty cases
			if (!K.is_empty() && !G.is_empty()){
				// Determine classification
				if (K.ub() <= 1 && K.lb() >= G.ub()){
					if (verbose > 1) std::cout << "swing pair valid" << std::endl;
					num_pair_inside_classifications++;
				} else if (K.lb() > 1 || K.ub() < G.lb()){
					num_pair_outside_classifications++;
					if (verbose > 1) std::cout << "swing pair not valid: " << std::endl;
				} else{
					if (t.diam()>0.0001){
						t_list.push_back(ibex::Interval(t.lb(),t.mid()));
						t_list.push_back(ibex::Interval(t.mid(),t.ub()));
						if (verbose > 1) std::cout << "BISECTION" << std::endl;
					} else{
						if (verbose > 1) std::cout << "BOUNDARY" << std::endl;
						has_boundary = true;
					}
				}
			}
		}

		if (verbose > 1) std::cout << "num_pair_inside_classifications "  << num_pair_inside_classifications << std::endl;
		if (verbose > 1) std::cout << "num_pair_outside_classifications "  << num_pair_outside_classifications << std::endl;

		if (num_pair_inside_classifications>0 && num_pair_outside_classifications==0 && !has_boundary){
			// swing pair constraint is valid for all t.
			num_inside_classifications++;
			if (verbose > 1) std::cout << "swing pair valid "  << std::endl;
		}
		else if (num_pair_outside_classifications>0  && num_pair_inside_classifications==0 && !has_boundary){
			// swing pair constraint is invalid for some t.
			num_outside_classifications++;
			if (verbose > 1) std::cout << "swing pair not valid " << std::endl;
		} else if (has_boundary || (num_pair_inside_classifications>0 && num_pair_outside_classifications>0)) {
			// if both inside and outside classifications are found then there must exist a boundary (there seems to be a rare
			// case where the bisection does not detect the boundary but does find inside and outside classifications)
			has_boundary = true;
			if (verbose > 1) std::cout << "swing pair partially valid "  << std::endl;
			break;
		} else {
			if (verbose > 1) std::cout << "swing pair empty" << std::endl;
		}

	}

	if (verbose > 0) std::cout << "num_inside_classifications "  << num_inside_classifications << std::endl;
	if (verbose > 0) std::cout << "num_outside_classifications "  << num_outside_classifications << std::endl;
	// Determine swing classification
	if (num_outside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing not valid "  << std::endl;
		return -1; // outside
	} else if (num_inside_classifications>0 && !has_boundary){
		if (verbose > 0) std::cout << "swing valid "  << std::endl;
		return 1; // inside
	} else{
		if (verbose > 0) std::cout << "swing partially valid "  << std::endl;
		return 0; // boundary
	}
}

int kinematics_existence(ibex::System &system_kinematics, ibex::IntervalVector &kinematics_box, int verbose){
	// Existence function
	ibex::Interval d3=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d3")];
	ibex::Interval d5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(), "d5")];
	ibex::Interval rx5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rx5")];
	ibex::Interval ry5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"ry5")];
	ibex::Interval rz5=kinematics_box[find_var_index(system_kinematics.f_ctrs.args(),"rz5")];

	ibex::Interval wrist_dist = sqrt(sqr(rx5) + sqr(ry5) + sqr(rz5));
	if (verbose > 1) std::cout << "wrist_dist: " << wrist_dist << std::endl;
	if (verbose > 1) std::cout << "d3 + d5: " << d3 + d5 << std::endl;
	if (wrist_dist.ub() < (d3 + d5).lb()){
		return 1;
	} else if (wrist_dist.lb() > (d3 + d5).ub()){
		return -1;
	} else {
		return 0;
	}

}














