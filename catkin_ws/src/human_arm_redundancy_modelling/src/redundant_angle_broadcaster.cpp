#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Float32.h>
#include "std_msgs/String.h"
#include "human_arm_redundancy_modelling/Redundancy.h"
#include <cmath>

int main(int argc, char** argv){
  ros::init(argc, argv, "tf2_listener");

  ros::NodeHandle node;

  ros::Publisher redundancies_pub = node.advertise<human_arm_redundancy_modelling::Redundancy>("redundancies",10);

  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);
  ros::Time now = ros::Time(0);

  ros::Rate rate(100.0);
  while (node.ok()){
    geometry_msgs::TransformStamped T02, T03, T08;
    try{
	  T02 = tfBuffer.lookupTransform("base_link", "shoulder_2", now, ros::Duration(1.0));
      T03 = tfBuffer.lookupTransform("base_link", "elbow_3", now, ros::Duration(1.0));
      T08 = tfBuffer.lookupTransform("base_link", "hand_8", now, ros::Duration(1.0));
    }
    catch (tf2::TransformException &ex) {
      ROS_WARN("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }

	human_arm_redundancy_modelling::Redundancy redundancies;

	// Calculate the elbow swivel angle

	tf2::Matrix3x3 R02(tf2::Quaternion(T02.transform.rotation.x,
										T02.transform.rotation.y,
										T02.transform.rotation.z,
										T02.transform.rotation.w));

	tf2::Matrix3x3 R03(tf2::Quaternion(T03.transform.rotation.x,
										T03.transform.rotation.y,
										T03.transform.rotation.z,
										T03.transform.rotation.w));

	tf2::Vector3 u2 = R02.getColumn(0);
    tf2::Vector3 w2 = R02.getColumn(2);
	tf2::Vector3 u3 = R03.getColumn(0);

	double cos = u3.dot(u2);
	double sin = u3.dot(-w2);

	redundancies.elbow_swivel = std::atan2(sin, cos);

	// Calculate the hand twist angle

	tf2::Matrix3x3 R08(tf2::Quaternion(T08.transform.rotation.x,
										T08.transform.rotation.y,
										T08.transform.rotation.z,
										T08.transform.rotation.w));

	tf2::Vector3 w8 = R08.getColumn(2);
	tf2::Vector3 u8 = R08.getColumn(0);
	tf2::Vector3 twist_axis = R08.getColumn(1);
	tf2::Vector3 twist_refaxis(1,0,0);

	double AX = twist_axis[0];
	double AY = twist_axis[1];
	double AZ = twist_axis[2];
	double gX = twist_refaxis[0];
	double gY = twist_refaxis[1];
	double gZ = twist_refaxis[2];
	double Rwx8 = w8[0];
	double Rwy8 = w8[1];
	double Rwz8 = w8[2];

	geometry_msgs::Point twist_point_value;
	geometry_msgs::Point twist_axis_value;
	geometry_msgs::Point twist_refaxis_value;

	twist_point_value.x = T08.transform.translation.x;
	twist_point_value.y = T08.transform.translation.y;
	twist_point_value.z = T08.transform.translation.z;

	twist_axis_value.x = AX;
	twist_axis_value.y = AY;
	twist_axis_value.z = AZ;

	twist_refaxis_value.x = gX;
	twist_refaxis_value.y = gY;
	twist_refaxis_value.z = gZ;


	double A0 = sqrt(pow(AX,2)+pow(AY,2)+pow(AZ,2));
	double A1 = sqrt(pow((AY*gZ-AZ*gY),2)+pow((-AX*gZ+AZ*gX),2)+pow((AX*gY-AY*gX),2));
	double A2 = sqrt(pow(((-AY*gY-AZ*gZ)*AX+gX*(pow(AY,2)+pow(AZ,2))),2)+pow((gY*(pow(AX,2)+pow(AZ,2))-AY*(AX*gX+AZ*gZ)),2)+pow((gZ*(pow(AX,2)+pow(AY,2))-AZ*(AX*gX+AY*gY)),2));
	double A3 = A1*A2;

	double sphi = (Rwy8 * (AX * AY * gY + AX * AZ * gZ - AY * AY * gX - AZ * AZ * gX) * A3 + A3 * AX * Rwx8 * (AX * gY - AY * gX) - A3 * AY * AZ * Rwx8 * gZ + A3 * AZ * AZ * Rwx8 * gY) / (-A2 * AX * AX * AZ * gY * gY - A2 * AX * AX * AZ * gZ * gZ + 2 * A2 * AX * AY * AZ * gX * gY + 2 * A2 * AX * AZ * AZ * gX * gZ - A2 * AY * AY * AZ * gX * gX - A2 * AY * AY * AZ * gZ * gZ + 2 * A2 * AY * AZ * AZ * gY * gZ - A2 * (int) pow((double) AZ, (double) 3) * gX * gX - A2 * (int) pow((double) AZ, (double) 3) * gY * gY);

	double cphi = (Rwz8 * (AY * gZ - AZ * gY) * A3 - A3 * AX * Rwx8 * gY + A3 * AY * Rwx8 * gX) / (A1 * AX * AX * AY * gY * gY + A1 * AX * AX * AY * gZ * gZ - 2 * A1 * AX * AY * AY * gX * gY - 2 * A1 * AX * AY * AZ * gX * gZ + A1 * (int) pow((double) AY, (double) 3) * gX * gX + A1 * (int) pow((double) AY, (double) 3) * gZ * gZ - 2 * A1 * AY * AY * AZ * gY * gZ + A1 * AY * AZ * AZ * gX * gX + A1 * AY * AZ * AZ * gY * gY);	

	redundancies.hand_twist = std::atan2(sphi, cphi);
	redundancies.twist_point = twist_point_value;
	redundancies.twist_axis = twist_axis_value;
	redundancies.twist_refaxis = twist_refaxis_value;

	// Publish values
	redundancies_pub.publish(redundancies);

	//ros::spinOnce();
    rate.sleep();
  }
  return 0;
};
