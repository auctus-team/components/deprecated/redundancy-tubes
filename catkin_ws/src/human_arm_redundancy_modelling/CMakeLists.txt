cmake_minimum_required(VERSION 2.8.3)
project(human_arm_redundancy_modelling)
set(CMAKE_VERBOSE_MAKEFILE ON)

find_package(PkgConfig REQUIRED)
pkg_search_module(PKG_KCADL REQUIRED kcadl)  # this looks for kcadl.pc file

find_package(catkin 
  REQUIRED COMPONENTS 
  roscpp 
  rospy 
  std_msgs 
  geometry_msgs 
  tf 
  message_generation
)

OPTION (USE_OpenMP "Use OpenMP" ON)
IF(USE_OpenMP)
  FIND_PACKAGE(OpenMP)
  IF(OPENMP_FOUND)
    SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  ENDIF()
ENDIF()

add_message_files(
  FILES
  Redundancy.msg
  RedundancyBox.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
  geometry_msgs
  sensor_msgs
)

catkin_package(CATKIN_DEPENDS message_runtime)

install(DIRECTORY launch DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
  PATTERN "setup_assistant.launch" EXCLUDE)
install(DIRECTORY config DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

include_directories(${catkin_INCLUDE_DIRS} /opt/ros/melodic/include /usr/local/include${PKG_KCADL_INCLUDE_DIRS})
link_directories(${catkin_LIB_DIRS} /usr/local/lib)

add_executable(redundant_angle_broadcaster src/redundant_angle_broadcaster.cpp)
target_link_libraries(redundant_angle_broadcaster ${catkin_LIBRARIES} ${PKG_KCADL_LDFLAGS})
add_dependencies(redundant_angle_broadcaster ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

add_executable(ros_redundancy_IK_solver src/ROS_redundancy_IK_solver.cpp)
add_dependencies(ros_redundancy_IK_solver ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_compile_options(ros_redundancy_IK_solver PRIVATE ${OpenMP_FLAGS} -std=c++11 -frounding-math)
target_link_libraries(ros_redundancy_IK_solver ${catkin_LIBRARIES} ${PKG_KCADL_LDFLAGS})

add_executable(Redundant_workspace src/Redundant_workspace.cpp)
add_dependencies(Redundant_workspace ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_compile_options(Redundant_workspace PRIVATE ${OpenMP_FLAGS} -std=c++11 -frounding-math) 
target_link_libraries(Redundant_workspace ${catkin_LIBRARIES} ${OpenMP_LIBS} ${PKG_KCADL_LDFLAGS})

