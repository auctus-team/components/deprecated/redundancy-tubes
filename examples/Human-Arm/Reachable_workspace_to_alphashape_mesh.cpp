/*
 * Reachable_workspace_to_alphashape_mesh.cpp
 *
 *  Created on: Apr 16, 2019
 *      Author: jpickard
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Alpha_shape_cell_base_3.h>
#include <CGAL/Alpha_shape_vertex_base_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "ibex.h"
#include <list>
#include <cassert>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Gt;
typedef CGAL::Tag_true                                      Alpha_cmp_tag;
//We use CGAL::Default to skip the complete declaration of base classes
typedef CGAL::Alpha_shape_vertex_base_3<Gt,CGAL::Default,Alpha_cmp_tag>   Vb;
typedef CGAL::Alpha_shape_cell_base_3<Gt,CGAL::Default,Alpha_cmp_tag>     Fb;
typedef CGAL::Triangulation_data_structure_3<Vb,Fb>         Tds;
typedef CGAL::Delaunay_triangulation_3<Gt,Tds>              Triangulation_3;
//Alpha shape with ExactAlphaComparisonTag set to true (note that the tag is also
//set to true for Vb and Fb)
typedef CGAL::Alpha_shape_3<Triangulation_3,Alpha_cmp_tag>  Alpha_shape_3;
typedef Gt::Point_3  										Point;
typedef Alpha_shape_3::Cell_handle                          Cell_handle;
typedef Alpha_shape_3::Vertex_handle                        Vertex_handle;
typedef Alpha_shape_3::Facet                                Facet;
typedef Alpha_shape_3::Edge                                 Edge;

int main() {
	// generating points on a grid.
	std::vector<Point> P_wrist;
	std::vector<Point> P_shoulder;
	uint fileNum = 1;
	bool files_exist = true;
	while (files_exist) {
		std::string filename = "manifold/list/manifold";
		filename += std::to_string(fileNum) + ".txt";
//      std::cout << filename << std::endl;
		std::ifstream file;
		file.open(filename);
		if (file.good()) {
			std::string line;
			int lineNum = 1;
			int varNum = 0;
			std::vector<std::string> list_variables;
			std::vector<ibex::Interval> list_values;
			while (std::getline(file, line)) {
				std::istringstream iss(line);
				if (lineNum == 3) {
					// Read all variables into list
					std::string word;
					while (iss >> word) {
						list_variables.push_back(word);
						varNum++;
					}
				} else if (lineNum == 7) {
					// Read all interval values into list
					double lb, ub;
					int varCounter = 1;
					while (iss >> lb >> ub && varCounter <= varNum) {
						ibex::Interval value(lb, ub);
						list_values.push_back(value);
						varCounter++;
					}
				}
				lineNum++;
			}

			// Get wrist point
			ibex::Interval d3 = list_values[std::distance(
								list_variables.begin(),
								std::find(list_variables.begin(), list_variables.end(),
										"d3"))];
			ibex::Interval rx4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rx4"))];
			ibex::Interval ry4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"ry4"))];
			ibex::Interval rz4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rz4"))];
			ibex::Interval rx7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rx7"))];
			ibex::Interval ry7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"ry7"))];
			ibex::Interval rz7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rz7"))];

//		  std::cout << '[' << rx4 << '\t' << ry4 << '\t' << rz4 << ']' << std::endl;
//		  std::cout << '[' << rx7 << '\t' << ry7 << '\t' << rz7 << ']' << std::endl;

			P_wrist.push_back(Point(rx7.mid(), ry7.mid(), rz7.mid()));

			// Enforce length of r4
			ibex::Vector r4(3);
			r4[0] = rx4.mid();
			r4[1] = ry4.mid();
			r4[2] = rz4.mid();
			double norm = ibex::norm(r4);
			r4 = 1/norm * r4;
			r4 = d3.mid() * r4;
			P_shoulder.push_back(Point(r4[0], r4[1], r4[2]));
			P_shoulder.push_back(Point(r4[0]/1.01, r4[1]/1.01, r4[2]/1.01));

		} else {
			files_exist = false;
		}
		fileNum++;
	}
	P_shoulder.push_back(Point(0.0, 0.0, 0.0));

	// Write OFF files for alpha shapes
	for(int shape=1; shape<=2; shape++){

		// Get points
		std::vector<Point> P;
		std::ofstream to_off; // Write exterior boundary of Alpha_shape_3 (surface triangulation) in Object File Format (OFF)
		double alpha_rad = 100;
		if (shape==1){
			P = P_wrist;
			to_off.open("manifold/wrist_workspace.off");
			alpha_rad = 101.0;
		}else if (shape==2){
			P = P_shoulder;
			to_off.open("manifold/shoulder_workspace.off");
			alpha_rad = 425.0;
		}

		//Build alpha_shapes in REGULARIZED mode
		Alpha_shape_3 as(P.begin(), P.end(), alpha_rad);

		/// collect all regular facets
		std::vector<Alpha_shape_3::Facet> facets;
		as.get_alpha_shape_facets(std::back_inserter(facets), Alpha_shape_3::REGULAR);
		std::cout << facets.size() << " boundary facets" << std::endl;

		std::stringstream pts;
		std::stringstream ind;

		std::size_t nbf=facets.size();
		for (std::size_t i=0;i<nbf;++i)
		{
		  //To have a consistent orientation of the facet, always consider an exterior cell
		  if ( as.classify( facets[i].first )!=Alpha_shape_3::EXTERIOR )
			facets[i]=as.mirror_facet( facets[i] );
		  CGAL_assertion(  as.classify( facets[i].first )==Alpha_shape_3::EXTERIOR  );

		  int indices[3]={
			(facets[i].second+1)%4,
			(facets[i].second+2)%4,
			(facets[i].second+3)%4,
		  };

		  /// according to the encoding of vertex indices, this is needed to get
		  /// a consistent orienation
		  if ( facets[i].second%2==0 ) std::swap(indices[0], indices[1]);


		  pts <<
		  facets[i].first->vertex(indices[0])->point() << "\n" <<
		  facets[i].first->vertex(indices[1])->point() << "\n" <<
		  facets[i].first->vertex(indices[2])->point() << "\n";
          if (shape==1)
		    ind << "3 " << 3*i << " " << 3*i+1 << " " << 3*i+2 << " 0.000" << " 0.000" << " 1.000" << " 0.500" << "\n";
          else if (shape==2)
            ind << "3 " << 3*i << " " << 3*i+1 << " " << 3*i+2 << " 0.000" << " 1.000" << " 0.000" << " 0.500" << "\n";
		}

		to_off << "OFF "<< 3*nbf << " " << nbf << " 0\n";
		to_off << pts.str();
		to_off << ind.str();
	}

	return 0;
}

