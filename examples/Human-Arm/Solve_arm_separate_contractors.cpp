/*
 * Solve_arm_swing_twist_constraints.cpp
 *
 *  Created on: Dec 11, 2018
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>

int main() {
	int numSegments = 8;
	int numQuaternionSegments = 5;

	ibex::Interval d3 = 28.74; //Length of upper arm
	ibex::Interval d5 = 27.15; //Length of forearm
	ibex::Interval d8 = 3.76; //Length of hand

	ibex::IntervalVector quaternionThetas(numQuaternionSegments);
	ibex::IntervalVector quaternionGammas(numQuaternionSegments);
	quaternionThetas[0] = ibex::Interval(1.0472);
	quaternionThetas[1] = ibex::Interval(1.0472);
	quaternionThetas[2] = ibex::Interval(1.0472);
	quaternionThetas[3] = ibex::Interval(1.0472);
	quaternionThetas[4] = ibex::Interval(1.0472);
	quaternionGammas[0] = ibex::Interval(-M_PI);
	quaternionGammas[1] = ibex::Interval(-M_PI/2);
	quaternionGammas[2] = ibex::Interval(0);
	quaternionGammas[3] = ibex::Interval(M_PI/2);
	quaternionGammas[4] = ibex::Interval(M_PI);

	// ################################################
	// CREATE KINEMATIC CONSTRAINTS
	// ################################################
	ibex::SystemFactory factory_kinematics;

	// Add variables to factory
	ibex::Variable Rux[numSegments];
	ibex::Variable Ruy[numSegments];
	ibex::Variable Ruz[numSegments];
	ibex::Variable Rvx[numSegments];
	ibex::Variable Rvy[numSegments];
	ibex::Variable Rvz[numSegments];
	ibex::Variable Rwx[numSegments];
	ibex::Variable Rwy[numSegments];
	ibex::Variable Rwz[numSegments];
	ibex::Variable rx[numSegments];
	ibex::Variable ry[numSegments];
	ibex::Variable rz[numSegments];
	ibex::Variable psi, phi;
	ibex::Variable t[numQuaternionSegments];
	ibex::Variable lsw, lsc, rc, uX, uY, uZ, vX, vY, vZ;

	for(int segment=0; segment<numSegments; segment++){
		// Special cases:
		if (segment==0){
			factory_kinematics.add_var(Rux[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Ruy[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Ruz[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvx[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvy[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvz[segment],ibex::Interval(-1,1));
			// Z axis of first segment equal to global Z axis
			factory_kinematics.add_var(Rwx[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rwy[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rwz[segment],ibex::Interval(1));
			// Position of first segment at global origin
			factory_kinematics.add_var(rx[segment],ibex::Interval(0));
			factory_kinematics.add_var(ry[segment],ibex::Interval(0));
			factory_kinematics.add_var(rz[segment],ibex::Interval(0));
		}
//		else if (segment==3){
//			// fix elbow swivel
//			factory_kinematics.add_var(Rux[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Ruy[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Ruz[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rvx[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rvy[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rvz[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rwx[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rwy[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(Rwz[segment],ibex::Interval(-1,1));
//			factory_kinematics.add_var(rx[segment],ibex::Interval::ALL_REALS);
//			factory_kinematics.add_var(ry[segment],ibex::Interval::ALL_REALS);
//			factory_kinematics.add_var(rz[segment],ibex::Interval(0));
//		}
		else if (segment==7){
			// pose of the hand
			factory_kinematics.add_var(Rux[segment],ibex::Interval(1));
			factory_kinematics.add_var(Ruy[segment],ibex::Interval(0));
			factory_kinematics.add_var(Ruz[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rvx[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rvy[segment],ibex::Interval(1));
			factory_kinematics.add_var(Rvz[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rwx[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rwy[segment],ibex::Interval(0));
			factory_kinematics.add_var(Rwz[segment],ibex::Interval(1));
			factory_kinematics.add_var(rx[segment],ibex::Interval(20));
			factory_kinematics.add_var(ry[segment],ibex::Interval(-20));
			factory_kinematics.add_var(rz[segment],ibex::Interval(20));
		}
		else {
			factory_kinematics.add_var(Rux[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Ruy[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Ruz[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvx[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvy[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rvz[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rwx[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rwy[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(Rwz[segment],ibex::Interval(-1,1));
			factory_kinematics.add_var(rx[segment],ibex::Interval::ALL_REALS);
			factory_kinematics.add_var(ry[segment],ibex::Interval::ALL_REALS);
			factory_kinematics.add_var(rz[segment],ibex::Interval::ALL_REALS);
		}
	}
	for(int segment=0; segment<numQuaternionSegments; segment++){
		factory_kinematics.add_var(t[segment],ibex::Interval(0,0));
	}
	factory_kinematics.add_var(psi,ibex::Interval(-M_PI,-M_PI)); /* swivel angle  */
	factory_kinematics.add_var(phi,ibex::Interval(0,0)); /* twist angle  */
	factory_kinematics.add_var(lsw,ibex::Interval::POS_REALS); /* swivel shoulder to wrist length */
	factory_kinematics.add_var(lsc,ibex::Interval::POS_REALS); /* swivel shoulder to circle centre length */
	factory_kinematics.add_var(rc,ibex::Interval::POS_REALS); /* swivel circle radius */
	factory_kinematics.add_var(uX,ibex::Interval(-1,1));
	factory_kinematics.add_var(uY,ibex::Interval(-1,1));
	factory_kinematics.add_var(uZ,ibex::Interval(-1,1));
	factory_kinematics.add_var(vX,ibex::Interval(-1,1));
	factory_kinematics.add_var(vY,ibex::Interval(-1,1));
	factory_kinematics.add_var(vZ,ibex::Interval(-1,1));

	// Add constraints to factory
	ibex::IntervalVector factors1(7,1);
	factors1[1] = d3 - 1;
	factors1[3] = d5 - 1;
	factors1[6] = d8 - 1;
	ibex::IntervalVector factors2(7,1);
	factors2[0] = -1;
	factors2[2] = -1;
	factors2[4] = -1;
	for(int segment=0; segment<numSegments-1; segment++){
		factory_kinematics.add_ctr(rx[segment] + Rvx[segment] * factors1[segment] = rx[segment+1] - Rwx[segment+1]);
		factory_kinematics.add_ctr(ry[segment] + Rvy[segment] * factors1[segment] = ry[segment+1] - Rwy[segment+1]);
		factory_kinematics.add_ctr(rz[segment] + Rvz[segment] * factors1[segment] = rz[segment+1] - Rwz[segment+1]);
		factory_kinematics.add_ctr(sqr(Rux[segment]) + sqr(Ruy[segment]) + sqr(Ruz[segment]) = 1);
		factory_kinematics.add_ctr(sqr(Rvx[segment]) + sqr(Rvy[segment]) + sqr(Rvz[segment]) = 1);
		factory_kinematics.add_ctr(Rux[segment] * Rvx[segment] + Ruy[segment] * Rvy[segment] + Ruz[segment] * Rvz[segment] = 0);
		factory_kinematics.add_ctr(Ruy[segment] * Rvz[segment] - Ruz[segment] * Rvy[segment] = Rwx[segment]);
		factory_kinematics.add_ctr(-Rux[segment] * Rvz[segment] + Ruz[segment] * Rvx[segment] = Rwy[segment]);
		factory_kinematics.add_ctr(Rux[segment] * Rvy[segment] - Ruy[segment] * Rvx[segment] = Rwz[segment]);
	}
	for(int segment=0; segment<numSegments-2; segment++){
		factory_kinematics.add_ctr(factors2[segment] * Rvx[segment] = Rwx[segment+1]);
		factory_kinematics.add_ctr(factors2[segment] * Rvy[segment] = Rwy[segment+1]);
		factory_kinematics.add_ctr(factors2[segment] * Rvz[segment] = Rwz[segment+1]);
	}
	factory_kinematics.add_ctr(Rux[numSegments-2] = Rux[numSegments-1]);
	factory_kinematics.add_ctr(Ruy[numSegments-2] = Ruy[numSegments-1]);
	factory_kinematics.add_ctr(Ruz[numSegments-2] = Ruz[numSegments-1]);
	factory_kinematics.add_ctr(Rvx[numSegments-2] = Rvx[numSegments-1]);
	factory_kinematics.add_ctr(Rvy[numSegments-2] = Rvy[numSegments-1]);
	factory_kinematics.add_ctr(Rvz[numSegments-2] = Rvz[numSegments-1]);
	factory_kinematics.add_ctr(Rwx[numSegments-2] = Rwx[numSegments-1]);
	factory_kinematics.add_ctr(Rwy[numSegments-2] = Rwy[numSegments-1]);
	factory_kinematics.add_ctr(Rwz[numSegments-2] = Rwz[numSegments-1]);

	// Build system
	ibex::System system_kinematics(factory_kinematics);
	std::cout << system_kinematics << std::endl;

	// Add contractors
	ibex::CtcHC4 system_kinematics_hc4(system_kinematics,0.01);
	ibex::CtcHC4 system_kinematics_hc4acid(system_kinematics,0.1,true);
	ibex::CtcAcid system_kinematics_acid(system_kinematics, system_kinematics_hc4acid);
	ibex::CtcCompo system_kinematics_contractor(system_kinematics_hc4,system_kinematics_acid);

	// ################################################
	// CREATE SWIVEL ANGLE CONSTRAINTS
	// ################################################
	ibex::SystemFactory factory_swivel;

	for(int segment=0; segment<numSegments; segment++){
		// Default:
		factory_swivel.add_var(Rux[segment]);
		factory_swivel.add_var(Ruy[segment]);
		factory_swivel.add_var(Ruz[segment]);
		factory_swivel.add_var(Rvx[segment]);
		factory_swivel.add_var(Rvy[segment]);
		factory_swivel.add_var(Rvz[segment]);
		factory_swivel.add_var(Rwx[segment]);
		factory_swivel.add_var(Rwy[segment]);
		factory_swivel.add_var(Rwz[segment]);
		factory_swivel.add_var(rx[segment]);
		factory_swivel.add_var(ry[segment]);
		factory_swivel.add_var(rz[segment]);
	}
	for(int segment=0; segment<numQuaternionSegments; segment++){
		factory_swivel.add_var(t[segment]);
	}
	factory_swivel.add_var(psi); /* swivel angle  */
	factory_swivel.add_var(phi); /* twist angle  */
	factory_swivel.add_var(lsw); /* swivel shoulder to wrist length */
	factory_swivel.add_var(lsc); /* swivel shoulder to circle centre length */
	factory_swivel.add_var(rc); /* swivel circle radius */
	factory_swivel.add_var(uX);
	factory_swivel.add_var(uY);
	factory_swivel.add_var(uZ);
	factory_swivel.add_var(vX);
	factory_swivel.add_var(vY);
	factory_swivel.add_var(vZ);

	// Add constraints to factory
//	lsw = sqrt((d8 ^ 2 + (-2 * Rwx8 * rx8 - 2 * Rwy8 * ry8 - 2 * Rwz8 * rz8) * d8 + rx8 ^ 2 + ry8 ^ 2 + rz8 ^ 2));
//	lsc = ((d8 ^ 2 + (-2 * Rwx8 * rx8 - 2 * Rwy8 * ry8 - 2 * Rwz8 * rz8) * d8 + rx8 ^ 2 + ry8 ^ 2 + rz8 ^ 2) + d3^2 - d5^2) / (2*lsw);
//	rc = sqrt(d3^2-lsc^2);
//	uX = -(rz7/lsw) * (rx7/lsw) * (1 + (rz7/lsw) ^ 4 + ((rx7/lsw) ^ 2 + (ry7/lsw) ^ 2 - 2) * (rz7/lsw) ^ 2) ^ (-0.1e1 / 0.2e1);
//	uY = -(rz7/lsw) * (ry7/lsw) * (1 + (rz7/lsw) ^ 4 + ((rx7/lsw) ^ 2 + (ry7/lsw) ^ 2 - 2) * (rz7/lsw) ^ 2) ^ (-0.1e1 / 0.2e1);
//	uZ = (-(rz7/lsw) ^ 2 + 1) * (1 + (rz7/lsw) ^ 4 + ((rx7/lsw) ^ 2 + (ry7/lsw) ^ 2 - 2) * (rz7/lsw) ^ 2) ^ (-0.1e1 / 0.2e1);
//	vX = (ry7/lsw) * uZ - (rz7/lsw) * uY;
//	vY = -(rx7/lsw) * uZ + (rz7/lsw) * uX;
//	vZ = (rx7/lsw) * uY - (ry7/lsw) * uX;
//	rx4 = ((rx7/lsw)*lsc) + rc*(cos(psi)*uX+sin(psi)*vX);
//	ry4 = ((ry7/lsw)*lsc) + rc*(cos(psi)*uY+sin(psi)*vY);
//	rz4 = ((rz7/lsw)*lsc) + rc*(cos(psi)*uZ+sin(psi)*vZ);
	factory_swivel.add_ctr(lsw = sqrt(sqr(rx[6]) + sqr(ry[6]) + sqr(rz[6])));
	factory_swivel.add_ctr(lsc = (sqr(lsw) + sqr(d3) - sqr(d5)) / (2*lsw));
	factory_swivel.add_ctr(rc = sqrt(sqr(d3)-sqr(lsc)));
	factory_swivel.add_ctr(uX = -(rz[6]/lsw) * (rx[6]/lsw) * pow((1 + pow((rz[6]/lsw),4) + (sqr(rx[6]/lsw) + sqr(ry[6]/lsw) - 2) * sqr(rz[6]/lsw)),(-0.1e1 / 0.2e1)));
	factory_swivel.add_ctr(uY = -(rz[6]/lsw) * (ry[6]/lsw) * pow((1 + pow((rz[6]/lsw),4) + (sqr(rx[6]/lsw) + sqr(ry[6]/lsw) - 2) * sqr(rz[6]/lsw)),(-0.1e1 / 0.2e1)));
	factory_swivel.add_ctr(uZ = (-sqr(rz[6]/lsw) + 1) * pow((1 + pow((rz[6]/lsw),4) + (sqr(rx[6]/lsw) + sqr(ry[6]/lsw) - 2) * sqr(rz[6]/lsw)),(-0.1e1 / 0.2e1)));
	factory_swivel.add_ctr(vX = (ry[6]/lsw) * uZ - (rz[6]/lsw) * uY);
	factory_swivel.add_ctr(vY = -(rx[6]/lsw) * uZ + (rz[6]/lsw) * uX);
	factory_swivel.add_ctr(vZ = (rx[6]/lsw) * uY - (ry[6]/lsw) * uX);
	factory_swivel.add_ctr(rx[3] = ((rx[6]/lsw)*lsc) + rc*(cos(psi)*uX+sin(psi)*vX));
	factory_swivel.add_ctr(ry[3] = ((ry[6]/lsw)*lsc) + rc*(cos(psi)*uY+sin(psi)*vY));
	factory_swivel.add_ctr(rz[3] = ((rz[6]/lsw)*lsc) + rc*(cos(psi)*uZ+sin(psi)*vZ));
//	factory_swivel.add_ctr(rx[3] = rx[6] * pow(lsw, -0.2e1) * (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5) / 0.2e1 + sqrt( (4 * d3 * d3) -  pow( (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5),  2) * pow(lsw, -0.2e1)) * (-cos(psi) * rz[6] * pow(lsw, -0.2e1) * rx[6] * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0) + sin(psi) * (ry[6] / lsw * (-rz[6] * rz[6] * pow(lsw, -0.2e1) + 0.1e1) * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0) + rz[6] * rz[6] * pow(lsw, -0.3e1) * ry[6] * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0))) / 0.2e1);
//	factory_swivel.add_ctr(ry[3] = ry[6] * pow(lsw, -0.2e1) * (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5) / 0.2e1 + sqrt( (4 * d3 * d3) -  pow( (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5),  2) * pow(lsw, -0.2e1)) * (-cos(psi) * rz[6] * pow(lsw, -0.2e1) * ry[6] * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0) + sin(psi) * (-rx[6] / lsw * (-rz[6] * rz[6] * pow(lsw, -0.2e1) + 0.1e1) * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0) - rz[6] * rz[6] * pow(lsw, -0.3e1) * rx[6] * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0))) / 0.2e1);
//	factory_swivel.add_ctr(rz[3] = rz[6] * pow(lsw, -0.2e1) * (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5) / 0.2e1 + sqrt( (4 * d3 * d3) -  pow( (d8 * d8 + (-2 * Rwx[7] * rx[7] - 2 * Rwy[7] * ry[7] - 2 * Rwz[7] * rz[7]) * d8 + rx[7] * rx[7] + ry[7] * ry[7] + rz[7] * rz[7] + d3 * d3 - d5 * d5),  2) * pow(lsw, -0.2e1)) * cos(psi) * (-rz[6] * rz[6] * pow(lsw, -0.2e1) + 0.1e1) * pow(0.1e1 + pow(rz[6], 0.4e1) * pow(lsw, -0.4e1) + (rx[6] * rx[6] * pow(lsw, -0.2e1) + ry[6] * ry[6] * pow(lsw, -0.2e1) - 0.2e1) * rz[6] * rz[6] * pow(lsw, -0.2e1), -0.5000000000e0) / 0.2e1);

	// Build system
	ibex::System system_swivel(factory_swivel);
	std::cout << system_swivel << std::endl;

	// Add contractors
	ibex::CtcHC4 system_swivel_hc4(system_swivel,0.01);
	ibex::CtcHC4 system_swivel_hc4acid(system_swivel,0.1,true);
	ibex::CtcAcid system_swivel_acid(system_swivel, system_swivel_hc4acid);
	ibex::CtcCompo system_swivel_contractor(system_swivel_hc4,system_swivel_acid);

	// ################################################
	// SOLVE SYSTEM WITH MULTIPLE CONSTRAINTS
	// ################################################
	// Create a smear-function bisection heuristic.
	ibex::SmearSumRelative system_kinematics_bisector(system_kinematics, 1e-02);

	// Create a "stack of boxes" (CellStack) (depth-first search).
	ibex::CellStack cellstack;

	// Push root cell
	cellstack.push(new ibex::Cell(system_kinematics.box));

	// Loop over cells in cellstack until empty
	while(!cellstack.empty()){
		// Apply contractions to top cell in cellstack
//		std::cout << "Contract" << std::endl;
		std::cout << cellstack.top()->box << std::endl;
		system_kinematics_contractor.contract(cellstack.top()->box);
		std::cout << cellstack.top()->box << std::endl;
		system_swivel_contractor.contract(cellstack.top()->box);
		std::cout << cellstack.top()->box << std::endl;
		sleep(1);
		// Bisection
		if (!cellstack.top()->box.is_empty()){
			// Pop cell and bisect
			ibex::Cell *cell = cellstack.pop();
			try{
				std::pair<ibex::Cell *,ibex::Cell *> bisection = system_kinematics_bisector.bisect(*cell);
				// Disp bisection
//				std::cout << "Bisected" << std::endl;
//				std::cout << bisection.first->box << std::endl;
//				std::cout << bisection.second->box << std::endl;
				cellstack.push(bisection.first);
				cellstack.push(bisection.second);
			} catch(...){
				// Display "solution"
				std::cout << "Solution" << std::endl;
				std::cout << cell->box << std::endl;
			}
		}
		else{
			// Pop cell to remove
			cellstack.pop();
		}
	}

	return 0;
}





