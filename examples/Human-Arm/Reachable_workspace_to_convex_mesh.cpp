/*
 * Reachable_workspace_to_convex_mesh.cpp
 *
 *  Created on: Apr 12, 2019
 *      Author: jpickard
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Random.h>
#include <vector>
#include <cassert>
#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "ibex.h"
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_3<K> Triangulation;
typedef Triangulation::Cell_handle Cell_handle;
typedef Triangulation::Vertex_handle Vertex_handle;
typedef Triangulation::Locate_type Locate_type;
typedef Triangulation::Point Point;

int main() {
	// generating points on a grid.
	std::vector<Point> P_wrist;
	uint fileNum = 1;
	bool files_exist = true;
	while (files_exist) {
		std::string filename = "manifold/list/manifold";
		filename += std::to_string(fileNum) + ".txt";
//      std::cout << filename << std::endl;
		std::ifstream file;
		file.open(filename);
		if (file.good()) {
			std::string line;
			int lineNum = 1;
			int varNum = 0;
			std::vector<std::string> list_variables;
			std::vector<ibex::Interval> list_values;
			while (std::getline(file, line)) {
				std::istringstream iss(line);
				if (lineNum == 3) {
					// Read all variables into list
					std::string word;
					while (iss >> word) {
						list_variables.push_back(word);
						varNum++;
					}
				} else if (lineNum == 7) {
					// Read all interval values into list
					double lb, ub;
					int varCounter = 1;
					while (iss >> lb >> ub && varCounter <= varNum) {
						ibex::Interval value(lb, ub);
						list_values.push_back(value);
						varCounter++;
					}
				}
				lineNum++;
			}

			// Get wrist point
			ibex::Interval rx4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rx4"))];
			ibex::Interval ry4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"ry4"))];
			ibex::Interval rz4 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rz4"))];
			ibex::Interval rx7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rx7"))];
			ibex::Interval ry7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"ry7"))];
			ibex::Interval rz7 = list_values[std::distance(
					list_variables.begin(),
					std::find(list_variables.begin(), list_variables.end(),
							"rz7"))];

//		  std::cout << '[' << rx4 << '\t' << ry4 << '\t' << rz4 << ']' << std::endl;
//		  std::cout << '[' << rx7 << '\t' << ry7 << '\t' << rz7 << ']' << std::endl;

			P_wrist.push_back(Point(rx7.mid(), ry7.mid(), rz7.mid()));

		} else {
			files_exist = false;
		}
		fileNum++;
	}

	// building the Delaunay triangulation.
	Triangulation T(P_wrist.begin(), P_wrist.end());

    // Write exterior boundary of Triangulation_3 (convex hull) in Object File Format (OFF)
	std::ofstream to_off("manifold/wrist_workspace.off"); // as a .off file

	// Build a map to assign an index to each vertex on the boundary of the triangulation (excluding interior and infinite).
	std::map<Triangulation::Vertex_handle, size_t> vertex_index_map;
	std::vector<Vertex_handle> boundary_vertices; // store them as we need to know their count before writing them out.
	size_t vertex_counter = 0;
	size_t number_of_facets = 0;
	for (Triangulation::All_cells_iterator c = T.cells_begin();
			c != T.cells_end(); c++) {
		if (!T.is_infinite(c))
			continue;

		++number_of_facets;

		for (size_t ind = 0; ind < 4; ++ind) {
			Triangulation::Vertex_handle vh = c->vertex(ind);
			if (T.is_infinite(vh))
				continue;

			auto ret = vertex_index_map.insert(
					std::pair<Triangulation::Vertex_handle, size_t>(vh,
							vertex_counter));
			if (ret.second) {
				boundary_vertices.push_back(vh);
				++vertex_counter;
			}
		}
	}

	// Write OFF header
	to_off << "OFF" << std::endl << boundary_vertices.size() << ' '
			<< number_of_facets << " 0" << std::endl;

	// Write vertices coordinates
	for (auto v : boundary_vertices) {
		to_off << v->point() << std::endl;
	}

	// Triangulation_3
	for (Triangulation::All_cells_iterator c = T.cells_begin();
			c != T.cells_end(); c++) {
		if (!T.is_infinite(c))
			continue;

		to_off << '3';

		// TODO: flip appropriates facets to ensure coherent orientations
		for (size_t ind = 0; ind < 4; ++ind) {
			Triangulation::Vertex_handle vh = c->vertex(ind);
			if (T.is_infinite(vh))
				continue;

			to_off << ' ' << vertex_index_map[vh];
		}

		to_off << std::endl;
	}

	return 0;
}

