/*
 * Solve_arm_twist_swivel_quaternion_constraints.cpp
 *
 *  Created on: Dec 10, 2018
 *      Author: jpickard
 */

#include "ibex.h"
#include "vibes.h"
#include <iostream>
#include <fstream>

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t);
ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi);
ibex::Interval norm(ibex::IntervalVector &v);

int main() {

	// ################################################
	// CREATE SOLVER
	// ################################################
	ibex::System system("examples/Human-Arm/minibex/Arm_kinematics.mbx");
	std::cout << system << std::endl;

	// Contractors
	ibex::CtcHC4 hc4(system,0.01);
	ibex::CtcHC4 hc4_2(system,0.1,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	/* Create a smear-function bisection heuristic. */
	ibex::SmearSumRelative bisector(system, 1e-02);

	/* Create a "stack of boxes" (CellStack) (depth-first search). */
	ibex::CellStack buff;

	/* Vector precisions required on variables */
	ibex::Vector prec(6, 1e-02);

	/* Create a solver with the previous objects */
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	// Quaternion constraints
	int num_points = 8;
	double _thetaswing[num_points] = {1.04719755119660,	2.26892802759263,	0.872664625997165,	0.523598775598299,	2.09439510239320,	1.74532925199433,	2.09439510239320,	1.04719755119660};
	double _phiswing[num_points] = {-3.14159265358979,	-2.79252680319093,	-1.91986217719376,	-0.872664625997165,	0.0,	0.872664625997165,	1.91986217719376,	3.14159265358979};
	ibex::IntervalVector thetaswing = ibex::IntervalVector(ibex::Vector(8, _thetaswing));
	ibex::IntervalVector phiswing = ibex::IntervalVector(ibex::Vector(8, _phiswing));

	// Lists
	std::vector<ibex::Interval> phiswing_list;
	std::vector<ibex::Interval> thetaswing_list;
	std::vector<ibex::Interval> g_list;

	// Check quaternion pairs
	for (int index=0; index<num_points-1; index++){
		ibex::IntervalVector qi(4);
		qi[0] = (cos((0.5)*sign(thetaswing[index])*thetaswing[index]));
		qi[1] = (sin(phiswing[index])*sin((0.5)*thetaswing[index]));
		qi[2] = (0);
		qi[3] = (cos(phiswing[index])*sin((0.5)*thetaswing[index]));

		ibex::IntervalVector qj(4);
		qj[0] = (cos((0.5)*sign(thetaswing[index+1])*thetaswing[index+1]));
		qj[1] = (sin(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));
		qj[2] = (0);
		qj[3] = (cos(phiswing[index+1])*sin((0.5)*thetaswing[index+1]));

		ibex::Interval t = 0;
		ibex::Interval dt = 1.0/75.0;
		while(t.ub()<=1.0){
			// Slerp to find intermediate quaternion
			ibex::IntervalVector qrel = slerp(qi, qj, t);

			// Convert quaternion to axis and angle
			ibex::IntervalVector u = qrel.subvector(1,3);
			ibex::Interval u_norm = norm(u);
			u = 1 / u_norm * qrel.subvector(1,3); // normalize
			ibex::Interval theta = 2 * atan2(u_norm, qrel[0]);
			ibex::Interval phi = atan2(u[0], u[2]);

//			std::cout << "theta: " << theta << std::endl;
//			std::cout << "phi: " << phi  << "\t" << t << std::endl;

			// Save theta and phi to list
			phiswing_list.push_back(phi);
			thetaswing_list.push_back(theta);
			g_list.push_back(cos(theta));

			// Increment t
			t += dt;
		}
	}

	// ################################################
	// EVALUATE SOLVER OVER PSI-PHI GRID
	// ################################################
	// Visualize results - plot kinematic chain
	vibes::beginDrawing();
	vibes::newFigure("Allowable Psi-Phi (Swivel-Twist)");
	vibes::axisLimits(-1,M_PI*2+1,-M_PI-1,M_PI+1);
	vibes::setFigureProperty("width",600);
	vibes::setFigureProperty("height",600);

	int spacing = 5;
	int solutionNum = 1;
	std::vector<ibex::IntervalVector> psiphiSolutions;
	for (int degpsi=0; degpsi<359; degpsi+=spacing){
		ibex::Interval radpsi;
		radpsi = static_cast<double>(degpsi)*M_PI/180;

		for (int degphi=-180; degphi<179; degphi+=spacing){
				ibex::Interval radphi;
				radphi = static_cast<double>(degphi)*M_PI/180;

				// Set psi and phi in system_box
				ibex::IntervalVector psiphi(2);
				psiphi[0] = radpsi;
				psiphi[1] = radphi;
				ibex::IntervalVector system_box = system.box;
				system_box[28] = radpsi;
				system_box[29] = radphi;

				// Check if system_box has a solution
				s.solve(system_box);
				std::cout << psiphi << std::endl;
				std::cout << s.get_manifold().size() << std::endl;
				if (s.get_manifold().size()>0){

					std::vector<ibex::QualifiedBox> solverSolutions;
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().inner.begin(), s.get_manifold().inner.end());
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().boundary.begin(), s.get_manifold().boundary.end());
					solverSolutions.insert(solverSolutions.end(), s.get_manifold().unknown.begin(), s.get_manifold().unknown.end());

					// For each solution, check quaternion constraints
					bool feasible = false;
					for (int isol=0; isol<1; isol++){
						// Continue until feasible solution found
						if (!feasible){
							ibex::IntervalVector sol_box = solverSolutions[isol].existence();

							// Compute K
							ibex::Interval d3 = 28.74;
							ibex::IntervalVector PEL_unit = 1/d3 * sol_box.subvector(25,27);
							ibex::IntervalVector a_vect(4,0);
							a_vect[2] = -1; // y axis
							ibex::IntervalVector a_vect_prime(4,0);
							a_vect_prime[1] = PEL_unit[0];
							a_vect_prime[2] = PEL_unit[1];
							a_vect_prime[3] = PEL_unit[2];
							ibex::Interval K = a_vect_prime * a_vect;
							ibex::IntervalVector U = ibex::cross(a_vect.subvector(1,3),a_vect_prime.subvector(1,3));
							ibex::Interval Anglephi = atan2(U[0],U[2]);
//							std::cout << "PEL_unit " << PEL_unit << std::endl;
//							std::cout << "a_vect_prime " << a_vect_prime << std::endl;
//							std::cout << "Anglephi " << Anglephi << std::endl;

							// Use Slerp to find thetaswing
							ibex::Interval gu = polygon_swing_xz_limit(Anglephi, thetaswing_list, phiswing_list);
							std::cout << gu << " <= " << K << " <= " << 1 << std::endl;

							// Feasible solution found!
							if (K.lb() >= gu.ub()){
								std::cout << "Feasible" << std::endl;
								feasible = true;
								psiphiSolutions.push_back(psiphi);
								vibes::drawEllipse (psiphi[0].mid(), psiphi[1].mid(), 0.01, 0.01, 0.0, "b");
							}
						}
					}
					// No feasible solution found
					if (!feasible){
						std::cout << "NOT feasible" << std::endl;
						vibes::drawEllipse (psiphi[0].mid(), psiphi[1].mid(), 0.01, 0.01, 0.0, "r");
					}
					// Save feasible solution
					else {
						// Save solution
						std:string file = "manifold/list/manifold";
						file += std::to_string(solutionNum);
						file += ".txt";
						std::cout << file << std::endl;
						const char *cstr = file.c_str();
						s.get_manifold().write_txt(cstr);
						solutionNum++;
					}
				}
		}
	}
	// Save psiphiSolutions to txt file
	std::ofstream myfile;
	myfile.open ("manifold/psi-phi-workspace.txt");
	for (int isol=0; isol<psiphiSolutions.size(); isol++){
		ibex::IntervalVector psiphi = psiphiSolutions[isol];
		myfile << psiphi[0].mid() << '\t' << psiphi[1].mid() << '\n';
	}
	myfile.close();


	return 0;
}

ibex::IntervalVector slerp(ibex::IntervalVector &q1, ibex::IntervalVector &q2, ibex::Interval t){
	// ################################################
	// SLERP ROUTINE TO GENERATE CONSTRAINT G
	// ################################################
	ibex::Interval dotqq = (q1[0]*q2[0]+q1[1]*q2[1]+q1[2]*q2[2]+q1[3]*q2[3]);
	ibex::Interval thetaqq = acos(dotqq);
	ibex::Interval scale0 = sin((1.0 - t) * acos(dotqq)) / sin(acos(dotqq));
	ibex::Interval scale1 = sin((t * acos(dotqq))) / sin(acos(dotqq));
	if (dotqq.ub()<0)
	scale1 = -scale1;
//	std::cout << "scale0 " << scale0 << std::endl;
//	std::cout << "scale1 " << scale1 << std::endl;
	ibex::IntervalVector q1q2t = scale0 * q1 + scale1 * q2;
	ibex::IntervalVector q1q2t_unit = 1/sqrt(sqr(q1q2t[0])+sqr(q1q2t[1])+sqr(q1q2t[2])+sqr(q1q2t[3])) * q1q2t;
	return q1q2t_unit;
}

ibex::Interval norm(ibex::IntervalVector &v){
	ibex::Interval norm_val = 0;
	for (int i=0; i<v.size(); i++)
		norm_val += sqr(v[i]);
	norm_val = sqrt(norm_val);
	return norm_val;
}

ibex::IntervalVector quatexp(ibex::IntervalVector &v){
	ibex::Interval v_norm = norm(v);
	ibex::IntervalVector quat(4);
	quat[0] = cos(v_norm);
	ibex::IntervalVector temp = sin(v_norm)/v_norm * v;
	quat[1] = temp[0];
	quat[2] = temp[1];
	quat[3] = temp[2];
	return quat;
}

ibex::Interval polygon_swing_xz_limit(ibex::Interval &phi, std::vector<ibex::Interval> vtheta,
		std::vector<ibex::Interval> vphi){

	// Find the indices of vphi which bound the value of phi
	int vphi_index1 = 0;
	int vphi_index2 = 0;
	for (int i=0; i<vphi.size(); i++){
		if (phi.lb() >= vphi[i].ub()){
			vphi_index1 = i;
			vphi_index2 = i+1;
		}
	}

	std::cout << "vphi_index1 " << vphi_index1 << std::endl;
	std::cout << "vphi_index2 " << vphi_index2 << std::endl;

	ibex::Interval phi1 = vphi[vphi_index1];
	ibex::Interval phi2 = vphi[vphi_index2];
	ibex::Interval theta1 = vtheta[vphi_index1];
	ibex::Interval theta2 = vtheta[vphi_index2];
//    std::cout << "phi1 " << phi1 << std::endl;
//    std::cout << "phi2 " << phi2 << std::endl;
//    std::cout << "theta1 " << theta1 << std::endl;
//    std::cout << "theta2 " << theta2 << std::endl;

	// Slerp
	ibex::Interval t = (phi - phi1) / (phi2 - phi1);
	ibex::IntervalVector u1(3);
	u1[0] = sin(phi1);
	u1[1] = 0;
	u1[2] = cos(phi1);
	ibex::IntervalVector v1 = theta1/2 * u1;
	ibex::IntervalVector q1 = quatexp(v1);

	ibex::IntervalVector u2(3);
	u2[0] = sin(phi2);
	u2[1] = 0;
	u2[2] = cos(phi2);
	ibex::IntervalVector v2 = theta2/2 * u2;
	ibex::IntervalVector q2 = quatexp(v2);

	ibex::IntervalVector qrel = slerp(q1,q2,t);

//	std::cout << "v1 " << v1 << std::endl;
//	std::cout << "v2 " << v2 << std::endl;
//	std::cout << "q1 " << q1 << std::endl;
//	std::cout << "q2 " << q2 << std::endl;
//	std::cout << "t " << t << std::endl;
//	std::cout << "qrel " << qrel << std::endl;

	// Swing limit (theta max)
	ibex::IntervalVector qrel_vect = qrel.subvector(1,3);
	ibex::Interval theta = 2 *atan2(norm(qrel_vect), qrel[0]);
	return cos(theta);
}





