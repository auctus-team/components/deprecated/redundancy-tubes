#!/usr/bin/env bash

stage_welcome()
{
  print_bold $1 "This program will install the redundancy-tubes project on your computer."
  sudo updatedb
  sudo apt-get install python-vtk
}

stage_kcadl_install()
{
    cd $FOLDER_START
    git submodule init 
    git submodule update 
    cd kcadl
    sudo ./install.sh
    mkdir build
    cd build
    cmake ../
    make 
    sudo make install
    cd $FOLDER_START
}

stage_make_doxygen()
{
  print_stage $1 $2 "Preparing documentation..."
  cd $FOLDER_START
  mkdir -p doc
  doxygen doc/doxygen.conf
  mv html doc/html
  mv latex doc/latex
}

update_LD_LIBRARY_PATH()
{
  print_stage $1 $2 "Updating LD_LIBRARY_PATH in ~/.bashrc"
  echo 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/kcadl/3rd/CGAL-4.13.1/build/lib/' >> ~/.bashrc 
}

stage_build_kcadl_install()
{
  print_stage $1 $2 "Building kcadl..."
  cd $FOLDER_START/examples
  make all
}

print_bold()
{
    echo -e "$(tput bold)$1$(tput sgr0)"
}

print_stage()
{
  print_bold "\n[$1/$2] $3"
}

prompt_str()
{
    read -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

prompt_str_pass()
{
    read -s -e -p "$(tput setaf 3)$1:$(tput sgr0) " str
    echo $str
}

echo "###################"
echo "#    INSTALLER    #"
echo "###################"
echo ""

STAGE_COUNT=4

PROJECT_NAME="redundancy-tubes"
FOLDER_START=$(pwd)
KCADL_3RD="/opt/kcadl/3rd/"
KCADL_BIN="$FOLDER_START/bin/"
KCADL_DOC="$FOLDER_START/doc/"

stage_welcome
stage_kcadl_install 1 $STAGE_COUNT
stage_make_doxygen 2 $STAGE_COUNT
update_LD_LIBRARY_PATH 3 $STAGE_COUNT
stage_build_kcadl_install 4 $STAGE_COUNT
echo -e "\nInstallation finished !\n"



